import { Knex } from 'knex';
import { tableName as userTableName } from '../migrations/20220403090054_create_users_table';

export async function seed(knex: Knex, users = 2): Promise<any[]> {
  // Deletes ALL existing entries
  await knex.raw(`truncate ${userTableName} cascade`);

  // Inserts seed entries
  return knex
    .batchInsert(
      userTableName,
      new Array(users).fill(0).map((_value, index) => {
        const increasedIndex = index + 1;
        return {
          email: `test_${increasedIndex}@gmail.com`,
          first_name: 'Test ' + increasedIndex,
          last_name: 'Test ' + increasedIndex,
        };
      }),
    )
    .returning('*');
}
