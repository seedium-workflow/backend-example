import { Knex } from 'knex';
import { seed as seedUsers } from './01_users';
import { seed as seedAccounts } from './02_accounts';
import { tableName as accountUserTableName } from '../migrations/20220403090102_create_account_users_table';

export async function seed(knex: Knex, count = 2): Promise<any[]> {
  const [users, accounts] = await Promise.all([
    seedUsers(knex, count),
    seedAccounts(knex, count),
  ]);

  // Deletes ALL existing entries
  await knex.raw(`truncate ${accountUserTableName} cascade`);

  // Inserts seed entries
  return knex(accountUserTableName)
    .insert(
      users.map((user, index) => ({
        user: user.id,
        account: accounts[index].id,
        role: 'owner',
      })),
    )
    .returning('*');
}
