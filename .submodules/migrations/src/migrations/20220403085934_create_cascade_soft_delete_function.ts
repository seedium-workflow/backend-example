import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  await knex.raw(`
create or replace function cascade_soft_delete()
returns trigger as $$
declare
  parent_column text := TG_ARGV[0];
begin
    for i in 1..(TG_NARGS-1) loop
      execute format('update %I.%I set deleted_at = $1 where %I = $2 and deleted_at is null', TG_TABLE_SCHEMA, TG_ARGV[i], parent_column) using new.deleted_at, old.id;
    end loop;
    return null;
end;
  $$ language plpgsql;
`);
}

export async function down(knex: Knex): Promise<void> {
  await knex.raw(`drop function cascade_soft_delete;`);
}
