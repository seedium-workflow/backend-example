import { Knex } from 'knex';
import { createSoftResourceColumns } from '../utils';

export const tableName = 'users';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable(tableName, (table) => {
    createSoftResourceColumns(knex, table, 'usr');
    table.string('sub');
    table.string('email').unique().notNullable();
    table.string('first_name');
    table.string('last_name');
    table.string('avatar_link');
    table.string('timezone');
    table.string('city');
    table.string('country');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable(tableName);
}
