import { Knex } from 'knex';
import {
  addCascadeSoftDeleteTrigger,
  dropCascadeSoftDeleteTrigger,
} from '../utils';
import { tableName as userTableName } from './20220403090054_create_users_table';
import { tableName as accountTableName } from './20220403090056_create_accounts_table';
import { tableName as accountUserTableName } from './20220403090102_create_account_users_table';
import { tableName as accountInvitationTableName } from './20220404173627_add_invitations_table';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.raw(`
${addCascadeSoftDeleteTrigger(userTableName, 'user', [accountUserTableName])}
${addCascadeSoftDeleteTrigger(accountTableName, 'account', [
  accountUserTableName,
  accountInvitationTableName,
])}
`);
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.raw(`
${dropCascadeSoftDeleteTrigger(userTableName)}
${dropCascadeSoftDeleteTrigger(accountTableName)}
`);
}
