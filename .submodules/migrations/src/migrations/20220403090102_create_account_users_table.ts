import { Knex } from 'knex';
import { tableName as userTableName } from './20220403090054_create_users_table';
import { tableName as accountTableName } from './20220403090056_create_accounts_table';
import {
  createEnumType,
  createSoftResourceColumns,
  dropEnumType,
  refTo,
} from '../utils';

export const tableName = 'account_users';
export const userAccountRoleEnum = `${tableName}_role_enum`;

export async function up(knex: Knex): Promise<void> {
  await knex.raw(createEnumType(userAccountRoleEnum, ['owner']));
  return knex.schema.createTable(tableName, (table) => {
    createSoftResourceColumns(knex, table, 'au');
    refTo(table, 'user', userTableName).notNullable();
    refTo(table, 'account', accountTableName).notNullable();
    table.specificType('role', userAccountRoleEnum).notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableName);
  return knex.raw(dropEnumType(userAccountRoleEnum));
}
