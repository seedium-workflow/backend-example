module.exports = {
  client: 'pg',
  connection: {
    host: process.env.DATABASE_HOST || 'localhost',
    port: process.env.DATABASE_PORT ? +process.env.DATABASE_PORT : 5432,
    user: process.env.DATABASE_USERNAME || 'postgres',
    password: process.env.DATABASE_PASSWORD || 'example',
    database: process.env.DATABASE_NAME || 'main',
    connectionString: process.env.DATABASE_URL,
  },
  pool: {
    min: 2,
    max: 10,
  },
  seeds: {
    extension: 'ts',
    directory: 'seeds',
  },
  migrations: {
    tableName: 'migrations',
    extension: 'ts',
    directory: 'migrations',
  },
};
