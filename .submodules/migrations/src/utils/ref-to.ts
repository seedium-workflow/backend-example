import { Knex } from 'knex';

export const refTo = (
  table: Knex.CreateTableBuilder | Knex.AlterTableBuilder,
  columnName: string,
  refTableName: string,
): Knex.ReferencingColumnBuilder => {
  return table
    .string(columnName, 30)
    .references('id')
    .inTable(refTableName)
    .onDelete('CASCADE');
};
