import { arrayToPostgresFormat } from './array-to-postgres-format';

export const addCascadeSoftDeleteTrigger = (
  tableName: string,
  parentColumnName: string,
  referenceTables: string[],
): string => `
create trigger cascade_soft_delete_${tableName}
    after update of deleted_at
    on ${tableName}
    for each row
    when(new.deleted_at is distinct from old.deleted_at and new.deleted_at is not null)
    execute procedure cascade_soft_delete('${parentColumnName}', ${arrayToPostgresFormat(
  referenceTables,
)});
`;

export const dropCascadeSoftDeleteTrigger = (tableName: string): string => `
drop trigger if exists cascade_soft_delete_${tableName} on ${tableName} cascade;
`;
