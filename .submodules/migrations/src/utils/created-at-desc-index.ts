export const createdAtIndexDesc = (tableName: string): string =>
  `create index ${tableName}_created_at_index on ${tableName} (created_at desc);`;

export const dropCreatedAtIndexDesc = (tableName: string): string =>
  `drop index ${tableName}_created_at_index;`;
