import { arrayToPostgresFormat } from './array-to-postgres-format';

export const createEnumType = (name: string, values: string[]): string =>
  `create type ${name} as enum (${arrayToPostgresFormat(values)});`;

export const dropEnumType = (name: string): string => `drop type ${name};`;
