import { Knex } from 'knex';
import { refTo } from './ref-to';
import { createSoftResourceColumns } from './create-soft-resource-columns';

export const createAccountResourceColumns = (
  knex: Knex,
  table: Knex.CreateTableBuilder,
  prefix: string,
  length?: number,
): void => {
  createSoftResourceColumns(knex, table, prefix, length);
  refTo(table, 'account', 'accounts').notNullable();
};
