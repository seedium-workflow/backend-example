export const arrayToPostgresFormat = (values: string[]): string =>
  values.map((value) => `'${value}'`).join(', ');
