import { Knex } from 'knex';
import { timestamp } from './timestamp';

export const createSoftResourceColumns = (
  knex: Knex,
  table: Knex.CreateTableBuilder,
  prefix: string,
  length = 16,
): void => {
  table
    .string('id', 30)
    .unique()
    .primary()
    .defaultTo(knex.raw(`random_prefix_id('${prefix}', ${length})`));
  table.integer('created_at').notNullable().defaultTo(timestamp(knex));
  table.integer('updated_at').notNullable().defaultTo(timestamp(knex));
  table.integer('deleted_at');
};
