import { NestFastifyApplication } from '@nestjs/platform-fastify';
import * as faker from 'faker';
import { UserModule } from '@app/user/user.module';
import { UserRecord } from '@app/user/interfaces';
import { AccountRecord } from '@app/account/interfaces';
import { ScopesConfig, scopesConfig } from '@config/scopes.config';
import { clearAll } from '../../core/lib/database/repositories';
import { buildApplication } from '../../core/lib/application';
import { insertRandomUserWithAccount } from '../../core/seeds';
import { buildHttpRequest, HttpRequest } from '../../core/lib/http-request';
import { expect, expectStatusCode } from '../../core/asserts';

describe('Profile API', () => {
  let app: NestFastifyApplication;
  let request: HttpRequest;
  let testUser: UserRecord;
  let testAccount: AccountRecord;
  before(async () => {
    app = await buildApplication({
      imports: [UserModule],
    });
  });
  beforeEach(async () => {
    const { user, account, token } = await insertRandomUserWithAccount();
    request = buildHttpRequest(app, {
      jwt: token,
    });
    testUser = user;
    testAccount = account;
  });
  afterEach(async () => {
    await clearAll();
  });
  after(async () => {
    await app.close();
  });
  describe('GET /profile/current_user', () => {
    it('should return user data', async () => {
      const { response, payload } = await request('/profile/current_user');
      expectStatusCode(response, 200);
      expect(payload).property('user').excluding(['sub']).deep.eq(testUser);
    });
    it('should return list of available accounts', async () => {
      const { response, payload } = await request('/profile/current_user');
      expectStatusCode(response, 200);
      expect(payload)
        .property('accounts')
        .property('data')
        .deep.eq([testAccount]);
    });
  });
  describe('GET /profile/current_account', async () => {
    it('should return account data', async () => {
      const { response, payload } = await request('/profile/current_account');
      expectStatusCode(response, 200);
      expect(payload).property('account').deep.eq(testAccount);
    });
    it('should return permission array', async () => {
      const sc = app.get<ScopesConfig>(scopesConfig.KEY);
      const ownerPermissions = [...sc.default, ...sc.roles.owner];
      const { response, payload } = await request('/profile/current_account');
      expectStatusCode(response, 200);
      expect(payload).property('permissions').deep.eq(ownerPermissions);
    });
  });
  describe('PATCH /profile', () => {
    it('should update profile', async () => {
      const fixtureProfileUpdateBody = {
        first_name: faker.name.firstName(),
        last_name: faker.name.lastName(),
        avatar_link: faker.image.image(),
        timezone: faker.address.timeZone(),
        city: faker.address.city(),
        country: faker.address.country(),
      };
      const { response, payload } = await request('/profile', {
        method: 'PATCH',
        payload: fixtureProfileUpdateBody,
      });
      expectStatusCode(response, 200);
      expect(payload)
        .excluding(['sub'])
        .deep.eq({
          ...testUser,
          ...fixtureProfileUpdateBody,
        });
    });
  });
});
