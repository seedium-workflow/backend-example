import { Get } from '@nestjs/common';
import { RestController } from '@modules/core/decorators';
import { AuthenticatedUser, AuthProtected } from '@app/auth/decorators';
import { User } from '@app/user/interfaces';

@RestController('auth-class', AuthProtected())
export class AuthControllerClassProtectedMock {
  @Get('protected')
  public async bearerProtected(@AuthenticatedUser() user: User): Promise<User> {
    return user;
  }
}
