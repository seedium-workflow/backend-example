import { Controller, Get } from '@nestjs/common';
import { RestMethod } from '@modules/core/decorators';
import { AuthenticatedUser, AuthProtected } from '@app/auth/decorators';
import { User } from '@app/user/interfaces';

@Controller('auth-method')
export class AuthControllerMethodMock {
  @Get('allow')
  @RestMethod({ scopes: [] })
  public async allow(): Promise<{ allowed: boolean }> {
    return { allowed: true };
  }

  @Get('protected')
  @RestMethod({ scopes: [] }, AuthProtected())
  public async protected(@AuthenticatedUser() user: User): Promise<User> {
    return user;
  }
}
