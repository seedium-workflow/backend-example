import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { AuthModule } from '@app/auth/auth.module';
import { UserModule } from '@app/user/user.module';
import { AccountUserModule } from '@app/relations/account-user/account-user.module';
import { buildApplication } from '../../core/lib/application';
import { clearAll } from '../../core/lib/database/repositories';
import { buildHttpRequest, HttpRequest } from '../../core/lib/http-request';
import { expectStatusCode, expectErrorCode } from '../../core/asserts';
import { getMockJwt } from '../../core/mocks';
import { insertRandomUser } from '../../core/seeds';
import {
  AuthControllerMethodMock,
  AuthControllerClassProtectedMock,
} from './mock';

describe('Auth Guards', () => {
  let app: NestFastifyApplication;
  let request: HttpRequest;
  before(async () => {
    app = await buildApplication({
      imports: [AuthModule, UserModule, AccountUserModule],
      controllers: [AuthControllerMethodMock, AuthControllerClassProtectedMock],
    });
  });
  beforeEach(async () => {
    request = buildHttpRequest(app);
  });
  afterEach(async () => {
    await clearAll();
  });
  after(async () => {
    await app.close();
  });

  describe('Class', () => {
    it('should return error not authenticated if no token are provided', async () => {
      const { response, payload } = await request<{ code: string }>(
        '/auth-class/protected',
      );
      expectStatusCode(response, 401);
      expectErrorCode(payload, 'not_authenticated_request');
    });
    it('should allow request with authenticated token', async () => {
      const testUser = await insertRandomUser();
      const { response } = await request('/auth-class/protected', {
        jwt: await getMockJwt(testUser.sub as string),
      });
      expectStatusCode(response, 200);
    });
  });

  describe('Methods', () => {
    it('should allow any request', async () => {
      const { response } = await request('/auth-method/allow');
      expectStatusCode(response, 200);
    });
    it('should allow request with authenticated token', async () => {
      const testUser = await insertRandomUser();
      const { response } = await request('/auth-method/protected', {
        jwt: await getMockJwt(testUser.sub as string),
      });
      expectStatusCode(response, 200);
    });
    it('should return error not authenticated if no token are provided', async () => {
      const { response, payload } = await request<{ code: string }>(
        '/auth-method/protected',
      );
      expectStatusCode(response, 401);
      expectErrorCode(payload, 'not_authenticated_request');
    });
  });
});
