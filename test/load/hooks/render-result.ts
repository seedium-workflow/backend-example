import autocannon from 'autocannon';
export const testCaseResults: { testCase: string; result: any }[] = [];

after(() => {
  testCaseResults.forEach(({ testCase, result }) => {
    process.stdout.write('Test case result for ' + testCase + '\n');
    process.stdout.write(
      autocannon.printResult(result, {
        renderResultsTable: true,
        renderLatencyTable: false,
      }),
    );
  });
});
