import { Knex } from 'knex';
import { Pool, PoolClient } from 'pg';
import {
  generateDatabaseConnectionOptions,
  getKnexManager,
} from '../../core/lib/database/knex';

export const knex: Knex = getKnexManager();
export let pool: Pool;
export let client: PoolClient;

before(async () => {
  pool = new Pool(generateDatabaseConnectionOptions());
  client = await pool.connect();
});

after(async () => {
  await client.release();
  await pool.end();
});
