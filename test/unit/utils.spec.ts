import * as chai from 'chai';
import * as faker from 'faker';
import { diff } from '@utils';

const expect = chai.expect;

describe('Utils', () => {
  describe('diff', () => {
    it('should return diff object for simple objects', () => {
      const previousString = faker.random.word();
      const newString = faker.random.word();
      const result = diff({ foo: previousString }, { foo: newString });
      expect(result).deep.eq({
        foo: newString,
      });
    });
    it('should not return if nested object was not changed', () => {
      const string = faker.random.word();
      const result = diff({ foo: string }, { foo: string });
      expect(result).deep.eq({});
    });
    it('should return diff for nested objects', () => {
      const firstFakeObject = {
        foo: faker.random.word(),
      };
      const secondFakeObject = {
        foo: faker.random.word(),
      };
      const result = diff({ bar: firstFakeObject }, { bar: secondFakeObject });
      expect(result).deep.eq({
        bar: {
          foo: secondFakeObject.foo,
        },
      });
    });
  });
});
