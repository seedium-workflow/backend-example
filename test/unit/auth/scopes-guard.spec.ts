import { restore } from 'sinon';
import { ForbiddenException } from '@app/auth/exceptions';
import { expect } from '../../core/asserts';
import { stubScopesGuardClasses } from '../../core/stubs';

describe('Scopes Guard', () => {
  afterEach(() => {
    restore();
  });
  describe('User permissions', () => {
    it('should return true for user', () => {
      const { scopesGuard, executionContext } = stubScopesGuardClasses(
        {
          default: ['noun:verb'],
        },
        ['noun:verb'],
        { user: [] },
      );
      const result = scopesGuard.canActivate(executionContext);
      expect(result).is.true;
    });
    it('should throw forbidden error if permissions are missed', () => {
      const { scopesGuard, executionContext } = stubScopesGuardClasses(
        {
          default: [],
        },
        ['noun:verb'],
        { user: [] },
      );
      expect(() => scopesGuard.canActivate(executionContext)).throws(
        ForbiddenException,
      );
    });
    it('should attach default permissions to user type', () => {
      const { scopesGuard, executionContext } = stubScopesGuardClasses(
        {
          default: ['noun:verb'],
        },
        ['noun:verb'],
        { user: [] },
      );
      const result = scopesGuard.canActivate(executionContext);
      expect(result).is.true;
    });
  });
});
