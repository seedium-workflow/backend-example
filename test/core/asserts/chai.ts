import { expect, use as chaiUse } from 'chai';
import { default as chaiExclude } from 'chai-exclude';
import { default as chaiAsPromised } from 'chai-as-promised';
import { default as sinonChai } from 'sinon-chai';

chaiUse(chaiAsPromised);
chaiUse(chaiExclude);
chaiUse(sinonChai);

export { expect };
