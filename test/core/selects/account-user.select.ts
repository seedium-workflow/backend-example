import { AccountUserRecord } from '@app/relations/account-user/interfaces';
import { accountUserRepository } from '../lib/database/repositories';

export const selectAccountUserById = (
  id: string,
): Promise<AccountUserRecord> => {
  return accountUserRepository().where('id', id).first();
};

export const selectAccountUserByUser = (
  idUser: string,
): Promise<AccountUserRecord[]> => {
  return accountUserRepository().where('user', idUser);
};

export const selectAccountUserByAccount = (
  idAccount: string,
): Promise<AccountUserRecord[]> => {
  return accountUserRepository().where('account', idAccount);
};
