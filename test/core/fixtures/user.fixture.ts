import * as faker from 'faker';
import { UserRecord } from '@app/user/interfaces';
import { now } from '@utils';

export const getRandomUser = (
  userOverride: Partial<UserRecord> = {},
): UserRecord => ({
  id: faker.datatype.uuid(),
  created_at: now(),
  updated_at: now(),
  deleted_at: null,
  sub: 'auth0|sub_id',
  avatar_link: faker.internet.url(),
  email: faker.internet.email(),
  first_name: faker.name.firstName(),
  last_name: faker.name.lastName(),
  city: faker.address.city(),
  country: faker.address.country(),
  timezone: faker.address.timeZone(),
  ...userOverride,
});
