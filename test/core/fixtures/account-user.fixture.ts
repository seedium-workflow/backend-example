import * as faker from 'faker';
import { AccountUserRecord } from '@app/relations/account-user/interfaces';
import { now } from '@utils';

export const getRandomAccountUser = (
  accountUserOverride: Partial<AccountUserRecord> = {},
): AccountUserRecord => ({
  id: faker.datatype.uuid(),
  created_at: now(),
  updated_at: now(),
  deleted_at: null,
  account: faker.datatype.uuid(),
  user: faker.datatype.uuid(),
  role: 'owner',
  ...accountUserOverride,
});
