import * as faker from 'faker';
import { AccountRecord } from '@app/account/interfaces';
import { now } from '@utils';

export const getRandomAccount = (
  accountOverride: Partial<AccountRecord> = {},
): AccountRecord => ({
  id: faker.datatype.uuid(),
  created_at: now(),
  updated_at: now(),
  deleted_at: null,
  name: faker.random.word(),
  ...accountOverride,
});
