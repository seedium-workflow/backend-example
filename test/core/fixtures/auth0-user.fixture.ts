import * as faker from 'faker';
import {
  Auth0RuleUserDto,
  Auth0EventRequestDto,
  Auth0EventUserPreSignupDto,
  Auth0EventUserPostSignupDto,
} from '@app/auth/interfaces';
import { HttpRequestOptions } from '../lib/http-request';

export const getRandomAuth0RuleUser = (
  auth0RuleUserOverride: Record<string, unknown> = {},
): Auth0RuleUserDto => ({
  _id: faker.random.word(),
  clientID: faker.random.word(),
  created_at: new Date().toString(),
  updated_at: new Date().toString(),
  email: faker.internet.email(),
  email_verified: true,
  family_name: faker.name.lastName(),
  given_name: faker.name.firstName(),
  identities: [],
  locale: 'en',
  name: faker.name.findName(),
  nickname: faker.name.findName(),
  picture: faker.image.imageUrl(),
  user_id: faker.random.word(),
  global_client_id: faker.random.word(),
  persistent: {},
  app_metadata: {},
  ...auth0RuleUserOverride,
});

interface IDatabaseSignUpRequestOptions extends HttpRequestOptions {
  payload: {
    user: Auth0EventUserPreSignupDto | Auth0EventUserPostSignupDto;
    request: Auth0EventRequestDto;
  };
}

export const getRandomDatabaseSignUpRequestOptions = (
  userPayload?: Auth0EventUserPreSignupDto | Auth0EventUserPostSignupDto,
): IDatabaseSignUpRequestOptions =>
  ({
    method: 'POST',
    payload: {
      user: userPayload,
      request: {
        ip: faker.internet.ip(),
        geoip: {
          cityName: faker.address.cityName(),
          countryName: faker.address.country(),
          latitude: parseFloat(faker.address.latitude()),
          longitude: parseFloat(faker.address.longitude()),
          timeZone: faker.address.timeZone(),
        },
        language: 'en,ru-RU;q=0.9,ru;q=0.8,en-US;q=0.7,uk;q=0.6',
      },
    },
    headers: {
      'X-API-Key': 'test_integration',
      'content-type': 'application/json',
    },
  } as IDatabaseSignUpRequestOptions);
