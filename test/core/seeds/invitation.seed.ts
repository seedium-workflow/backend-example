import { InvitationRecord } from '@app/account/interfaces';
import { invitationRepository } from '../lib/database/repositories';
import { getRandomInvitation } from '../fixtures';
import { getIdOrCreateNewResource } from '../utils';
import { insertRandomAccount } from './account.seed';

export const insertRandomInvitation = async ({
  account,
  ...invitationOverride
}: Partial<InvitationRecord> = {}): Promise<InvitationRecord> => {
  const idAccount = await getIdOrCreateNewResource(account, () =>
    insertRandomAccount(),
  );
  const [invitation] = await invitationRepository()
    .insert(
      getRandomInvitation({
        id: undefined,
        account: idAccount,
        ...invitationOverride,
      }),
    )
    .returning('*');
  return invitation;
};
