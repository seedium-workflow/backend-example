import { AccountRecord } from '@app/account/interfaces';
import { accountRepository } from '../lib/database/repositories';
import { getRandomAccount } from '../fixtures';

export const insertRandomAccount = async (
  accountOverride: Partial<AccountRecord> = {},
): Promise<AccountRecord> => {
  const [account] = await accountRepository()
    .insert(
      getRandomAccount({
        id: undefined,
        ...accountOverride,
      }),
    )
    .returning('*');
  return account;
};
