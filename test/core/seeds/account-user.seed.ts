import { AccountUserRecord } from '@app/relations/account-user/interfaces';
import { accountUserRepository } from '../lib/database/repositories';
import { getRandomAccountUser } from '../fixtures';
import { getIdOrCreateNewResource } from '../utils';
import { insertRandomAccount } from './account.seed';
import { insertRandomUser } from './user.seed';

export const insertRandomAccountUser = async ({
  account,
  user,
  ...accountUserOverride
}: Partial<AccountUserRecord> = {}): Promise<AccountUserRecord> => {
  const idAccount = await getIdOrCreateNewResource(account, () =>
    insertRandomAccount(),
  );
  const idUser = await getIdOrCreateNewResource(user, () => insertRandomUser());
  const [accountUser] = await accountUserRepository()
    .insert(
      getRandomAccountUser({
        id: undefined,
        account: idAccount,
        user: idUser,
        ...accountUserOverride,
      }),
    )
    .returning('*');
  return accountUser;
};
