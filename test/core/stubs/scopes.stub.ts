import { stub } from 'sinon';
import { Reflector } from '@nestjs/core';
import { ExecutionContext } from '@nestjs/common';
import { ScopesConfig } from '@config/scopes.config';
import { ScopesGuard } from '@app/auth/guards';
import {
  EXPECTED_SCOPES_OPTIONS_TOKEN,
  EXPECTED_SCOPES_TOKEN,
} from '@app/auth/auth.constants';

export const stubScopesGuardClasses = (
  scopesConfig: Partial<ScopesConfig>,
  expectedScopes: string[],
  request = {},
  expectedScopesOptions = {},
): { scopesGuard: ScopesGuard; executionContext: ExecutionContext } => {
  const testHandler = function test() {
    return;
  };
  const reflectorGetStub = stub();
  reflectorGetStub
    .withArgs(EXPECTED_SCOPES_TOKEN, testHandler)
    .returns(expectedScopes);
  reflectorGetStub
    .withArgs(EXPECTED_SCOPES_OPTIONS_TOKEN, testHandler)
    .returns(expectedScopesOptions);
  return {
    scopesGuard: new ScopesGuard(
      {
        get: reflectorGetStub,
      } as unknown as Reflector,
      {
        default: [],
        roles: {},
        ...scopesConfig,
      } as unknown as ScopesConfig,
    ),
    executionContext: {
      getHandler: stub().returns(testHandler),
      switchToHttp: stub().returns({
        getRequest: stub().returns(request),
      }),
    } as unknown as ExecutionContext,
  };
};
