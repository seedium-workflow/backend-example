import { randomUUID } from 'crypto';
import { NestFactory } from '@nestjs/core';
import { NestApplicationContextOptions } from '@nestjs/common/interfaces/nest-application-context-options.interface';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { parse as qsParse } from 'qs';
import { default as fastifyCors } from 'fastify-cors';
import { Logger } from 'nestjs-pino';
import { verifyNodeJSVersion } from '@utils';
import { AppModule } from '@app/app.module';

export const setupApplication = async (
  options: NestApplicationContextOptions = {},
): Promise<NestFastifyApplication> => {
  await verifyNodeJSVersion();

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({
      disableRequestLogging: true,
      genReqId: () => randomUUID(),
      querystringParser: (s) => qsParse(s),
    }),
    {
      bufferLogs: true,
      ...options,
    },
  );

  app.useLogger(app.get(Logger));
  await app.register(fastifyCors, {
    credentials: true,
    origin: true,
    exposedHeaders: ['Authorization', 'Content-Type'],
  });
  app.enableShutdownHooks();
  app.setGlobalPrefix('v1');

  return app;
};
