import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { NestFastifyApplication } from '@nestjs/platform-fastify';

export const setupSwagger = (
  app: NestFastifyApplication,
  options: { version: string; environment: string },
): OpenAPIObject | void => {
  if (options.environment === 'local') {
    const swaggerConfig = new DocumentBuilder()
      .setTitle('Example')
      .setDescription('API description for Example project')
      .setVersion(options.version)
      .addBearerAuth()
      .addServer('http://localhost:3000', 'local')
      .addServer('https://api.dev.example.com', 'development')
      .addServer('https://api.example.com', 'production');

    const swaggerDocument = SwaggerModule.createDocument(
      app,
      swaggerConfig.build(),
      { deepScanRoutes: true },
    );

    SwaggerModule.setup('api/docs', app, swaggerDocument, {
      uiConfig: {
        deepLinking: true,
        displayRequestDuration: true,
        filter: true,
        showExtensions: true,
        showCommonExtensions: true,
        tryItOutEnabled: true,
        persistAuthorization: true,
      },
    });

    return swaggerDocument;
  }
};
