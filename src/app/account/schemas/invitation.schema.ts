import S from 'fluent-json-schema';
import { resourceObjectSchema } from '@lib/schemas';

export const invitationDtoSchema = S.object()
  .additionalProperties(false)
  .prop('email', S.string())
  .prop('token', S.string())
  .prop('role', S.string())
  .extend(resourceObjectSchema);

export const invitationPublicRetrieveDtoSchema = S.object()
  .additionalProperties(false)
  .prop('account_name', S.string())
  .extend(invitationDtoSchema);
