import S from 'fluent-json-schema';
import { defaultLengthStringSchema } from '@lib/schemas';

export const createAccountBodyDtoSchema = S.object()
  .additionalProperties(false)
  .prop('name', defaultLengthStringSchema);

export const updateAccountBodyDtoSchema = S.object()
  .additionalProperties(false)
  .prop('name', defaultLengthStringSchema);
