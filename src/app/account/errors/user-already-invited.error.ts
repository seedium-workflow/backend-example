export class UserAlreadyInvitedError extends Error {
  constructor() {
    super(`User already invited`);
  }
}
