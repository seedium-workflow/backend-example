export class WrongInvitationEmailError extends Error {
  constructor() {
    super(`Forbidden manage invitation`);
  }
}
