import {
  EntityRepository,
  ID,
  RepositoryDatabaseOptions,
  SoftAccountRepository,
} from 'nestjs-postgres';
import {
  InvitationRecord,
  Invitation,
  InvitationWithAccountNameRecord,
} from '../interfaces';

@EntityRepository({ name: 'invitations' })
export class InvitationRepository extends SoftAccountRepository<
  InvitationRecord,
  Invitation
> {
  public async retrieveByEmail(
    idAccount: ID,
    email: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<InvitationRecord | null> {
    return this._repository.hooks.retrieve(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select('*')
        .where('email', email)
        .where('account', idAccount)
        .whereNull('deleted_at')
        .first(),
    );
  }
  public async retrieveByToken(
    token: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<InvitationRecord | null> {
    return this._repository.hooks.retrieve(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select('*')
        .where('token', token)
        .whereNull('deleted_at')
        .first(),
    );
  }
  public async retrieveWithAccountNameByToken(
    token: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<InvitationWithAccountNameRecord | null> {
    const accountAlias = 'accounts';
    const invitationAlias = 'invitations';
    return this._repository.hooks.retrieve(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select(`${invitationAlias}.*`)
        .select(
          this._repository.rawBuilder()(`${accountAlias}.name as account_name`),
        )
        .where(`${invitationAlias}.token`, token)
        .whereNull(`${invitationAlias}.deleted_at`)
        .join(accountAlias, `${accountAlias}.id`, `${invitationAlias}.account`)
        .first(),
    );
  }
}
