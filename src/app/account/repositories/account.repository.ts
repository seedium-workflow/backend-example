import { SoftRepository, EntityRepository } from 'nestjs-postgres';
import { AccountRecord, Account } from '../interfaces';

@EntityRepository({ name: 'accounts' })
export class AccountRepository extends SoftRepository<AccountRecord, Account> {}
