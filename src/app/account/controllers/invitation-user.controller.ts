import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Get, Param, Post } from '@nestjs/common';
import { AuthenticatedUser, AuthProtected } from '@app/auth/decorators';
import { RestController, RestMethod } from '@modules/core/decorators';
import { ResourceMissingException } from '@lib/exceptions/api';
import { User } from '@app/user/interfaces';
import { Metadata, MetadataContainer } from '@modules/core/metadata';
import { WrongInvitationEmailError } from '@app/account/errors';
import { WrongInvitationEmailException } from '@app/account/exceptions';
import { httpAssert, isError } from '@utils';
import { invitationPublicRetrieveDtoSchema } from '../schemas/invitation.schema';
import { InvitationWithAccountNameRecord } from '../interfaces';
import { InvitationService } from '../services';

@ApiTags('Invitations')
@RestController('invitations/:token')
export class InvitationUserController {
  constructor(private readonly invitationService: InvitationService) {}

  @Get('retrieve')
  @ApiOperation({
    summary: 'Retrieve public information about invitation by token',
  })
  @RestMethod({
    responses: {
      200: {
        schema: invitationPublicRetrieveDtoSchema,
      },
    },
  })
  public async retrieve(
    @Param('token') token: string,
  ): Promise<InvitationWithAccountNameRecord> {
    const invitation =
      await this.invitationService.retrieveWithAccountNameByToken(token);
    httpAssert(invitation, new ResourceMissingException());
    return invitation;
  }

  @Post('accept')
  @ApiOperation({ summary: 'Accept invitation by token' })
  @RestMethod(
    {
      statusCode: 204,
    },
    AuthProtected(),
  )
  public async accept(
    @AuthenticatedUser() user: User,
    @Param('token') token: string,
    @Metadata() metadata: MetadataContainer,
  ): Promise<void> {
    const maybeError = await this.invitationService.accept(
      user,
      token,
      metadata,
    );
    httpAssert(maybeError, new ResourceMissingException());
    httpAssert(
      !(maybeError instanceof WrongInvitationEmailError),
      new WrongInvitationEmailException(
        'You are forbidden to accept the invitation',
      ),
    );
    httpAssert(!isError(maybeError), maybeError as Error);
  }

  @Post('reject')
  @ApiOperation({ summary: 'Reject invitation by token' })
  @RestMethod(
    {
      statusCode: 204,
    },
    AuthProtected(),
  )
  public async reject(
    @AuthenticatedUser() user: User,
    @Param('token') token: string,
    @Metadata() metadata: MetadataContainer,
  ): Promise<void> {
    const maybeError = await this.invitationService.reject(
      user,
      token,
      metadata,
    );
    httpAssert(maybeError, new ResourceMissingException());
    httpAssert(
      !(maybeError instanceof WrongInvitationEmailError),
      new WrongInvitationEmailException(
        'You are forbidden to reject the invitation',
      ),
    );
    httpAssert(!isError(maybeError), maybeError as Error);
  }
}
