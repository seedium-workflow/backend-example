import { randomUUID } from 'crypto';
import { Injectable } from '@nestjs/common';
import {
  ID,
  IList,
  IDataOptions,
  OmitDefaultResourceFields,
  SelectRepositoryDatabaseOptions,
  RepositoryListOptionsCreated,
  OmitDefaultAccountResourceFields,
  RepositoryDatabaseOptions,
} from 'nestjs-postgres';
import { TransactionOptions, TransactionService } from '@modules/transaction';
import { EventBus } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { diff } from '@utils';
import { User } from '@app/user/interfaces';
import { AccountUserService } from '@app/relations/account-user/services';
import {
  UserAlreadyInAccountError,
  UserAlreadyInvitedError,
  WrongInvitationEmailError,
} from '../errors';
import {
  InvitationCreatedEvent,
  InvitationDeletedEvent,
  InvitationUpdatedEvent,
} from '../events/impl';
import { InvitationRepository } from '../repositories';
import {
  Invitation,
  InvitationRecord,
  InvitationWithAccountNameRecord,
  CreateInvitationBodyDto,
} from '../interfaces';

@Injectable()
export class InvitationService {
  constructor(
    private readonly invitationRepository: InvitationRepository,
    private readonly accountUserService: AccountUserService,
    private readonly transactionService: TransactionService,
    private readonly eventBus: EventBus,
  ) {}
  public async accept(
    user: User,
    token: string,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<InvitationRecord | Error | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const invitation = await this.retrieveByToken(token, {
        transaction: trx.knexTrx,
      });
      if (!invitation) {
        return null;
      }
      if (invitation.email !== user.email) {
        return new WrongInvitationEmailError();
      }
      const [deletedInvitation] = await Promise.all([
        this.delete(invitation.account, invitation.id, metadata, trx),
        this.accountUserService.create(
          invitation.account,
          {
            user: user.id,
            role: invitation.role,
          },
          trx,
        ),
      ]);
      return deletedInvitation;
    }, transactionOptions);
  }
  public async reject(
    user: User,
    token: string,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<InvitationRecord | Error | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const invitation = await this.retrieveByToken(token, {
        transaction: trx.knexTrx,
      });
      if (!invitation) {
        return null;
      }
      if (invitation.email !== user.email) {
        return new WrongInvitationEmailError();
      }
      return this.delete(invitation.account, invitation.id, metadata, trx);
    }, transactionOptions);
  }
  public async createFromDto(
    idAccount: ID,
    createInvitationBodyDto: CreateInvitationBodyDto,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<InvitationRecord | Error> {
    const [maybeExistingUser, maybeExistingInvitation] = await Promise.all([
      this.accountUserService.retrieveByEmailInAccount(
        idAccount,
        createInvitationBodyDto.email,
        {
          transaction: transactionOptions?.knexTrx,
        },
      ),
      this.retrieveByEmail(idAccount, createInvitationBodyDto.email, {
        transaction: transactionOptions?.knexTrx,
      }),
    ]);
    if (maybeExistingUser) {
      return new UserAlreadyInAccountError();
    }
    if (maybeExistingInvitation) {
      return new UserAlreadyInvitedError();
    }
    return this.create(
      idAccount,
      {
        ...createInvitationBodyDto,
        role: createInvitationBodyDto.role ?? 'owner',
        token: randomUUID(),
      },
      metadata,
      transactionOptions,
    );
  }
  public async retrieveByEmail(
    idAccount: ID,
    email: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<InvitationRecord | null> {
    return this.invitationRepository.retrieveByEmail(
      idAccount,
      email,
      databaseOptions,
    );
  }
  public async retrieveByToken(
    token: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<InvitationRecord | null> {
    return this.invitationRepository.retrieveByToken(token, databaseOptions);
  }
  public async retrieveWithAccountNameByToken(
    token: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<InvitationWithAccountNameRecord | null> {
    return this.invitationRepository.retrieveWithAccountNameByToken(
      token,
      databaseOptions,
    );
  }
  public async list<T extends Invitation>(
    idAccount: ID,
    options?: RepositoryListOptionsCreated,
    databaseOptions?: SelectRepositoryDatabaseOptions<
      InvitationRecord,
      Invitation
    >,
  ): Promise<IList<T>> {
    return this.invitationRepository.list(idAccount, options, databaseOptions);
  }
  public async create(
    idAccount: ID,
    createAccountPayload: OmitDefaultAccountResourceFields<InvitationRecord>,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<InvitationRecord> {
    return this.transactionService.withTransaction(async (trx) => {
      const createdInvitation = await this.invitationRepository.create(
        idAccount,
        createAccountPayload,
        {
          transaction: trx.knexTrx,
        },
      );
      await this.eventBus.publish(
        new InvitationCreatedEvent(createdInvitation, metadata),
        {
          transaction: trx.eventBusTrx,
        },
      );
      return createdInvitation;
    }, transactionOptions);
  }
  public async retrieve<T extends Invitation>(
    idAccount: ID,
    idInvitation: ID,
    options?: IDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<
      InvitationRecord,
      Invitation
    >,
  ): Promise<T | null> {
    return this.invitationRepository.retrieve(
      idAccount,
      idInvitation,
      options,
      databaseOptions,
    );
  }
  public async update(
    idAccount: ID,
    idInvitation: ID,
    updateAccountPayload: Partial<OmitDefaultResourceFields<InvitationRecord>>,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<InvitationRecord | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const previousInvitation = await this.retrieve<InvitationRecord>(
        idAccount,
        idInvitation,
        undefined,
        {
          transaction: trx.knexTrx,
        },
      );
      if (!previousInvitation) {
        return null;
      }
      const updatedInvitation = await this.invitationRepository.update(
        idAccount,
        idInvitation,
        updateAccountPayload,
        {
          transaction: trx.knexTrx,
        },
      );
      if (!updatedInvitation) {
        return null;
      }
      await this.eventBus.publish(
        new InvitationUpdatedEvent(
          updatedInvitation,
          diff(updatedInvitation, previousInvitation),
          metadata,
        ),
        {
          transaction: trx.eventBusTrx,
        },
      );
      return updatedInvitation;
    }, transactionOptions);
  }
  public async delete(
    idAccount: ID,
    idInvitation: ID,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<InvitationRecord | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const deletedInvitation = await this.invitationRepository.delete(
        idAccount,
        idInvitation,
        {
          transaction: trx.knexTrx,
        },
      );
      if (!deletedInvitation) {
        return null;
      }
      await this.eventBus.publish(
        new InvitationDeletedEvent(deletedInvitation, metadata),
        {
          transaction: trx.eventBusTrx,
        },
      );
      return deletedInvitation;
    }, transactionOptions);
  }
}
