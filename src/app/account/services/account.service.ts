import { Injectable } from '@nestjs/common';
import {
  ID,
  IList,
  IDataOptions,
  OmitDefaultResourceFields,
  SelectRepositoryDatabaseOptions,
  RepositoryListOptionsCreated,
} from 'nestjs-postgres';
import { TransactionOptions, TransactionService } from '@modules/transaction';
import { EventBus } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { diff, isError } from '@utils';
import { AccountUserService } from '@app/relations/account-user/services';
import {
  AccountCreatedEvent,
  AccountDeletedEvent,
  AccountUpdatedEvent,
} from '../events/impl';
import { AccountRepository } from '../repositories';
import { Account, AccountRecord, CreateAccountBodyDto } from '../interfaces';

@Injectable()
export class AccountService {
  constructor(
    private readonly accountRepository: AccountRepository,
    private readonly accountUserService: AccountUserService,
    private readonly transactionService: TransactionService,
    private readonly eventBus: EventBus,
  ) {}
  public async createFromDto(
    createAccountBodyDto: CreateAccountBodyDto,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<AccountRecord | Error> {
    return this.transactionService.withTransaction(async (trx) => {
      const createdAccountOrError = await this.create(
        createAccountBodyDto,
        metadata,
        trx,
      );
      if (isError(createdAccountOrError)) {
        return createdAccountOrError;
      }
      await this.accountUserService.create(
        createdAccountOrError.id,
        {
          user: metadata.idUser(true),
          role: 'owner',
        },
        trx,
      );
      return createdAccountOrError;
    }, transactionOptions);
  }
  public async list<T extends Account>(
    options?: RepositoryListOptionsCreated,
    databaseOptions?: SelectRepositoryDatabaseOptions<AccountRecord, Account>,
  ): Promise<IList<T>> {
    return this.accountRepository.list(options, databaseOptions);
  }
  public async create(
    createAccountPayload: OmitDefaultResourceFields<AccountRecord>,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<AccountRecord | Error> {
    return this.transactionService.withTransaction(async (trx) => {
      const createdAccount = await this.accountRepository.create(
        createAccountPayload,
        {
          transaction: trx.knexTrx,
        },
      );
      await this.eventBus.publish(
        new AccountCreatedEvent(createdAccount, metadata),
        {
          transaction: trx.eventBusTrx,
        },
      );
      return createdAccount;
    }, transactionOptions);
  }
  public async retrieve<T extends Account>(
    idAccount: ID,
    options?: IDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<AccountRecord, Account>,
  ): Promise<T | null> {
    return this.accountRepository.retrieve(idAccount, options, databaseOptions);
  }
  public async update(
    idAccount: ID,
    updateAccountPayload: Partial<OmitDefaultResourceFields<AccountRecord>>,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<AccountRecord | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const previousAccount = await this.retrieve(idAccount, undefined, {
        transaction: trx.knexTrx,
      });
      if (!previousAccount) {
        return null;
      }
      const updatedAccount = await this.accountRepository.update(
        idAccount,
        updateAccountPayload,
        {
          transaction: trx.knexTrx,
        },
      );
      if (!updatedAccount) {
        return null;
      }
      await this.eventBus.publish(
        new AccountUpdatedEvent(
          updatedAccount,
          diff(updatedAccount, previousAccount),
          metadata,
        ),
        {
          transaction: trx.eventBusTrx,
        },
      );
      return updatedAccount;
    }, transactionOptions);
  }
  public async delete(
    idAccount: ID,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<AccountRecord | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const deletedAccount = await this.accountRepository.delete(idAccount, {
        transaction: trx.knexTrx,
      });
      if (!deletedAccount) {
        return null;
      }
      await this.eventBus.publish(
        new AccountDeletedEvent(deletedAccount, metadata),
        {
          transaction: trx.eventBusTrx,
        },
      );
      return deletedAccount;
    }, transactionOptions);
  }
}
