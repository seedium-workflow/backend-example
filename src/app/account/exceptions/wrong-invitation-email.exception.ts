import { AccountBaseException } from './account-base.exception';

export class WrongInvitationEmailException extends AccountBaseException {
  public code = 'wrong_invitation_email';
  public status = 400;
  constructor(message: string) {
    super(message);
  }
}
