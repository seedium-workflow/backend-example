import { AccountBaseException } from './account-base.exception';

export class AccountAlreadyExistsException extends AccountBaseException {
  code = 'account_already_exists';
  status = 400;
  constructor() {
    super('Account already exists');
  }
}
