import { AccountBaseException } from './account-base.exception';

export class UserAlreadyInAccountException extends AccountBaseException {
  public code = 'user_already_in_account';
  public status = 400;
  constructor(message: string) {
    super(message);
  }
}
