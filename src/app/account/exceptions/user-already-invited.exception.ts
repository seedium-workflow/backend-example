import { AccountBaseException } from './account-base.exception';

export class UserAlreadyInvitedException extends AccountBaseException {
  public code = 'user_already_invited';
  public status = 400;
  constructor(message: string) {
    super(message);
  }
}
