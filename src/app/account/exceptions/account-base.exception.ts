import { BaseException } from '@lib/exceptions/base.exception';

export abstract class AccountBaseException extends BaseException {
  public type = 'account_error';
}
