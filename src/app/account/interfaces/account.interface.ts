import { IResourceObject } from 'nestjs-postgres';

export interface Account extends IResourceObject {
  name: string;
}

export interface AccountRecord extends IResourceObject {
  name: string;
}
