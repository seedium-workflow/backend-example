import { IAccountRecordModel, IAccountResourceModel } from 'nestjs-postgres';
import { AccountUserRole } from '@app/relations/account-user/interfaces';

export interface Invitation extends IAccountResourceModel {
  email: string;
  token: string;
  role: AccountUserRole;
}

export interface InvitationRecord extends IAccountRecordModel {
  email: string;
  token: string;
  role: AccountUserRole;
}

export interface InvitationWithAccountNameRecord extends InvitationRecord {
  account_name: string;
}
