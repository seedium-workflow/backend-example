export interface CreateAccountBodyDto {
  name: string;
}

export interface UpdateAccountBodyDto {
  name?: string;
}
