import { AccountUserRole } from '@app/relations/account-user/interfaces';

export interface CreateInvitationBodyDto {
  email: string;
  role?: AccountUserRole;
}
