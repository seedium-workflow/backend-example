import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { InvitationRecord } from '../../interfaces';

export class InvitationCreatedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): InvitationCreatedEvent {
    const {
      id,
      data: { object },
      request,
    } = BaseEvent.parseEventMessage<InvitationRecord>(message);
    return new InvitationCreatedEvent(
      object,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }

  constructor(
    public readonly invitation: InvitationRecord,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }

  public toJson(): string {
    return super.toJson(this.invitation);
  }
}
