import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { InvitationRecord } from '../../interfaces';

export class InvitationUpdatedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): InvitationUpdatedEvent {
    const {
      id,
      request,
      data: { object, previous_attributes = {} },
    } = BaseEvent.parseEventMessage<InvitationRecord>(message);
    return new InvitationUpdatedEvent(
      object,
      previous_attributes,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }
  constructor(
    public readonly invitation: InvitationRecord,
    public readonly previousAttributes: Partial<InvitationRecord>,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }
  public toJson(): string {
    return super.toJson(this.invitation, this.previousAttributes);
  }
}
