import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { AccountRecord } from '../../interfaces';

export class AccountUpdatedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): AccountUpdatedEvent {
    const {
      id,
      request,
      data: { object, previous_attributes = {} },
    } = BaseEvent.parseEventMessage<AccountRecord>(message);
    return new AccountUpdatedEvent(
      object,
      previous_attributes,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }
  constructor(
    public readonly account: AccountRecord,
    public readonly previousAttributes: Partial<AccountRecord>,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }
  public toJson(): string {
    return super.toJson(this.account, this.previousAttributes);
  }
}
