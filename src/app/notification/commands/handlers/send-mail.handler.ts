import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { MailService } from '@modules/mail/services';
import { SendMailCommand } from '../impl';

@CommandHandler(SendMailCommand)
export class SendMailHandler implements ICommandHandler<SendMailCommand> {
  constructor(private readonly mailService: MailService) {}
  public async execute({ to, options }: SendMailCommand): Promise<void> {
    await this.mailService.send(to, options);
  }
}
