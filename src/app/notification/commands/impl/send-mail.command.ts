import { BaseCommand } from '@modules/cqrs';
import { SendOptions } from '@modules/mail/interfaces';
import { MetadataContainer } from '@modules/core/metadata';

export class SendMailCommand extends BaseCommand {
  constructor(
    public readonly to: string | string[],
    public readonly options: SendOptions,
    public readonly metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }
}
