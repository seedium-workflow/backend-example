import { Injectable } from '@nestjs/common';
import { MetadataContainer } from '@modules/core/metadata';
import { TransactionOptions, TransactionService } from '@modules/transaction';
import { ID, IDataOptions, OmitDefaultResourceFields } from 'nestjs-postgres';
import { UserEmailAlreadyExistsError } from '@app/user/errors';
import { EventBus } from '@modules/cqrs';
import {
  UserCreatedEvent,
  UserDeletedEvent,
  UserUpdatedEvent,
} from '@app/user/events/impl';
import { diff } from '@utils';
import {
  CompletedUser,
  NotCompletedUser,
  User,
  UserRecord,
  UserSignupCompletePayload,
  UserSignUpPayload,
} from '../interfaces';
import { UserRepository } from '../repositories';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly transactionService: TransactionService,
    private readonly eventBus: EventBus,
  ) {}
  public async signUp(
    userSignUpPayload: UserSignUpPayload,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<NotCompletedUser | Error> {
    return this.create(
      {
        email: userSignUpPayload.email,
        first_name: userSignUpPayload.first_name ?? null,
        last_name: userSignUpPayload.last_name ?? null,
        avatar_link: userSignUpPayload.avatar_link ?? null,
        city: userSignUpPayload.city ?? null,
        country: userSignUpPayload.country ?? null,
        timezone: userSignUpPayload.timezone ?? null,
        sub: null,
      },
      metadata,
      transactionOptions,
    );
  }
  public async finishSignUp(
    userSignUpPayload: UserSignupCompletePayload,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<CompletedUser | null> {
    const user = await this.retrieveByEmail(
      userSignUpPayload.email,
      metadata,
      transactionOptions,
    );
    if (!user) {
      return null;
    }
    return this.update(
      user.id,
      {
        first_name: userSignUpPayload.first_name ?? user.first_name,
        last_name: userSignUpPayload.last_name ?? user.last_name,
        avatar_link: userSignUpPayload.avatar_link ?? user.avatar_link,
        timezone: userSignUpPayload.timezone ?? user.timezone,
        city: userSignUpPayload.city ?? user.city,
        country: userSignUpPayload.country ?? user.country,
        sub: userSignUpPayload.sub,
      },
      metadata,
      transactionOptions,
    ) as Promise<CompletedUser>;
  }
  public async retrieveByEmail(
    email: string,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<User | null> {
    return this.userRepository.retrieveByEmail(email, {
      transaction: transactionOptions?.knexTrx,
    });
  }
  public async retrieveBySubjectId(
    sub: string,
    transactionOptions?: TransactionOptions,
  ): Promise<CompletedUser | null> {
    return this.userRepository.retrieveBySubjectId(sub, {
      transaction: transactionOptions?.knexTrx,
    });
  }
  public async create(
    user: OmitDefaultResourceFields<UserRecord>,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<UserRecord | Error> {
    try {
      return await this.transactionService.withTransaction(async (trx) => {
        const createdUser = await this.userRepository.create(user, {
          transaction: trx.knexTrx,
        });
        await this.eventBus.publish(
          new UserCreatedEvent(createdUser, metadata),
          {
            transaction: trx.eventBusTrx,
          },
        );
        return createdUser;
      }, transactionOptions);
    } catch (err) {
      if (err instanceof UserEmailAlreadyExistsError) {
        return err;
      }
      throw err;
    }
  }
  public async retrieve<T extends User>(
    idUser: ID,
    options?: IDataOptions,
    transactionOptions?: TransactionOptions,
  ): Promise<T | null> {
    return this.userRepository.retrieve(idUser, options, {
      transaction: transactionOptions?.knexTrx,
    });
  }
  public async update(
    idUser: ID,
    updateUserPayload: Partial<OmitDefaultResourceFields<UserRecord>>,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<UserRecord | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const previousUser = await this.retrieve(idUser, undefined, trx);
      if (!previousUser) {
        return null;
      }
      const updatedUser = await this.userRepository.update(
        idUser,
        updateUserPayload,
        {
          transaction: trx.knexTrx,
        },
      );
      if (!updatedUser) {
        return null;
      }
      await this.eventBus.publish(
        new UserUpdatedEvent(
          updatedUser,
          diff(updatedUser, previousUser),
          metadata,
        ),
        {
          transaction: trx.eventBusTrx,
        },
      );
      return updatedUser;
    }, transactionOptions);
  }

  public async delete(
    idUser: ID,
    metadata: MetadataContainer,
    transactionOptions?: TransactionOptions,
  ): Promise<UserRecord | null> {
    return this.transactionService.withTransaction(async (trx) => {
      const deletedUser = await this.userRepository.delete(idUser, {
        transaction: trx.knexTrx,
      });
      if (!deletedUser) {
        return null;
      }
      await this.eventBus.publish(new UserDeletedEvent(deletedUser, metadata), {
        transaction: trx.eventBusTrx,
      });
      return deletedUser;
    }, transactionOptions);
  }
}
