import { BaseException } from '@lib/exceptions/base.exception';

export abstract class UserBaseException extends BaseException {
  public type = 'user_error';
}
