import { AccountRecord } from '@app/account/interfaces';
import { IList } from '@lib/db';
import { UserRecord } from './user.interface';

export interface ProfileCurrentUserResponseDto {
  user: UserRecord;
  accounts: IList<AccountRecord>;
}

export interface ProfileCurrentAccountResponseDto {
  account: AccountRecord;
  permissions: string[];
}

export interface UpdateProfileBodyDto {
  first_name?: string;
  last_name?: string;
  avatar_link?: string;
  timezone?: string;
  city?: string;
  country?: string;
}
