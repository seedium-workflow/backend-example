import { IResourceObject } from 'nestjs-postgres';
import { Nullable } from '@lib/interfaces';

export interface BaseUser extends IResourceObject {
  email: string;
  first_name: Nullable<string>;
  last_name: Nullable<string>;
  avatar_link: Nullable<string>;
  timezone: Nullable<string>;
  city: Nullable<string>;
  country: Nullable<string>;
}

export interface NotCompletedUser extends BaseUser {
  sub: Nullable<string>;
}

export interface CompletedUser extends BaseUser {
  sub: string;
}

export type User = NotCompletedUser | CompletedUser;

export interface UserRecord extends IResourceObject {
  sub: Nullable<string>;
  email: string;
  first_name: Nullable<string>;
  last_name: Nullable<string>;
  avatar_link: Nullable<string>;
  timezone: Nullable<string>;
  city: Nullable<string>;
  country: Nullable<string>;
}
