import { Get, Patch, Body } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { Metadata, MetadataContainer } from '@modules/core/metadata';
import { RestController, RestMethod } from '@modules/core/decorators';
import { scopes } from '@config/scopes.config';
import {
  AuthProtected,
  AuthenticatedUser,
  Permissions,
} from '@app/auth/decorators';
import { ResourceMissingException } from '@lib/exceptions/api';
import { AccountUserService } from '@app/relations/account-user/services';
import { httpAssert } from '@utils';
import { UserService } from '../services';
import { userDtoSchema } from '../schemas/user.schema';
import {
  profileCurrentAccountResponseDtoSchema,
  profileCurrentUserResponseDtoSchema,
  updateProfileDtoSchema,
} from '../schemas/profile-controller.schema';
import {
  UpdateProfileBodyDto,
  CompletedUser,
  User,
  ProfileCurrentUserResponseDto,
  ProfileCurrentAccountResponseDto,
} from '../interfaces';

@ApiTags('Profile')
@RestController('profile', AuthProtected())
export class ProfileController {
  constructor(
    private readonly userService: UserService,
    private readonly accountUserService: AccountUserService,
  ) {}

  @Get('current_user')
  @ApiOperation({ summary: 'Retrieve profile' })
  @RestMethod({
    scopes: [scopes.profile.retrieve],
    responses: { 200: { schema: profileCurrentUserResponseDtoSchema } },
  })
  public async retrieveCurrentUser(
    @AuthenticatedUser() user: CompletedUser,
  ): Promise<ProfileCurrentUserResponseDto> {
    const [profile, accounts] = await Promise.all([
      this.userService.retrieve(user.id),
      this.accountUserService.listAccountsByUserId(user.id, { limit: 100 }),
    ]);
    httpAssert(profile, new ResourceMissingException());
    return {
      user: profile,
      accounts,
    };
  }

  @Get('current_account')
  @ApiOperation({ summary: 'Retrieve current active account' })
  @RestMethod({
    scopes: [scopes.profile.retrieve],
    responses: { 200: { schema: profileCurrentAccountResponseDtoSchema } },
  })
  public async retrieveCurrentAccount(
    @AuthenticatedUser() user: CompletedUser,
    @Permissions() permissions: string[],
  ): Promise<ProfileCurrentAccountResponseDto> {
    const {
      data: [account],
    } = await this.accountUserService.listAccountsByUserId(user.id, {
      limit: 1,
    });
    httpAssert(account, new ResourceMissingException());
    return {
      account,
      permissions,
    };
  }

  @Patch()
  @ApiOperation({ summary: 'Update profile' })
  @RestMethod({
    scopes: [scopes.profile.update],
    body: updateProfileDtoSchema,
    responses: { 200: { schema: userDtoSchema } },
  })
  public async update(
    @AuthenticatedUser() user: CompletedUser,
    @Body() updateProfileBody: UpdateProfileBodyDto,
    @Metadata() metadata: MetadataContainer,
  ): Promise<User> {
    const profile = await this.userService.update(
      user.id,
      updateProfileBody,
      metadata,
    );
    httpAssert(profile, new ResourceMissingException());
    return profile;
  }
}
