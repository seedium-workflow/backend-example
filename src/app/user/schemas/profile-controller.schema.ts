import S from 'fluent-json-schema';
import {
  defaultLengthStringSchema,
  listResponseSchema,
  nullable,
  urlSchema,
} from '@lib/schemas';
import { accountDtoSchema } from '@app/account/schemas/account.schema';
import { userDtoSchema } from './user.schema';

export const profileCurrentUserResponseDtoSchema = S.object()
  .additionalProperties(false)
  .prop('user', userDtoSchema)
  .prop('accounts', listResponseSchema(accountDtoSchema));

export const profileCurrentAccountResponseDtoSchema = S.object()
  .additionalProperties(false)
  .prop('account', accountDtoSchema)
  .prop('permissions', S.array().items(S.string()));

export const updateProfileDtoSchema = S.object()
  .additionalProperties(false)
  .prop('first_name', nullable(defaultLengthStringSchema))
  .prop('last_name', nullable(defaultLengthStringSchema))
  .prop('avatar_link', urlSchema)
  .prop('timezone', defaultLengthStringSchema)
  .prop('city', defaultLengthStringSchema)
  .prop('country', defaultLengthStringSchema);
