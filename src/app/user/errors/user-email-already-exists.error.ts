export class UserEmailAlreadyExistsError extends Error {
  constructor(email?: string) {
    super('User' + (email ? ` with email "${email}"` : '') + ' already exists');
  }
}
