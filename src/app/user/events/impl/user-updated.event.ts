import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { UserRecord } from '../../interfaces';

export class UserUpdatedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): UserUpdatedEvent {
    const {
      id,
      request,
      data: { object, previous_attributes = {} },
    } = BaseEvent.parseEventMessage<UserRecord>(message);
    return new UserUpdatedEvent(
      object,
      previous_attributes,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }
  constructor(
    public readonly user: UserRecord,
    public readonly previousAttributes: Partial<UserRecord>,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }
  public toJson(): string {
    return super.toJson(this.user, this.previousAttributes);
  }
}
