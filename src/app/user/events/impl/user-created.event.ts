import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { UserRecord } from '../../interfaces';

export class UserCreatedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): UserCreatedEvent {
    const {
      id,
      data: { object },
      request,
    } = BaseEvent.parseEventMessage<UserRecord>(message);
    return new UserCreatedEvent(
      object,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }

  constructor(
    public readonly user: UserRecord,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }

  public toJson(): string {
    return super.toJson(this.user);
  }
}
