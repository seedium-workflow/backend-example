import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { UserRecord } from '../../interfaces';

export class UserDeletedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): UserDeletedEvent {
    const {
      id,
      request,
      data: { object },
    } = BaseEvent.parseEventMessage<UserRecord>(message);
    return new UserDeletedEvent(
      object,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }
  constructor(
    public readonly user: UserRecord,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }
  public toJson(): string {
    return super.toJson(this.user);
  }
}
