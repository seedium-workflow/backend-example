import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';

export const AuthenticatedUser = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<ApplicationFastifyRequest>();
    return req.user ?? null;
  },
);
