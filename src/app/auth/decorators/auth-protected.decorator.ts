import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthenticationGuard, ScopesGuard } from '../guards';

export const AuthProtected = (): (<TFunction extends Function, Y>(
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void) => {
  return applyDecorators(
    ApiBearerAuth(),
    UseGuards(AuthenticationGuard, ScopesGuard),
  );
};
