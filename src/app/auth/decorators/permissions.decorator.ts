import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';

export const Permissions = createParamDecorator(
  (data: unknown, context: ExecutionContext): string[] => {
    const req = context.switchToHttp().getRequest<ApplicationFastifyRequest>();
    return req.permissions ?? [];
  },
);
