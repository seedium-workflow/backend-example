import S from 'fluent-json-schema';

export const auth0UserIdentity = S.object()
  .additionalProperties(false)
  .prop('connection', S.string())
  .prop('user_id', S.string())
  .prop('provider', S.string())
  .prop('isSocial', S.boolean());
