import S from 'fluent-json-schema';
import { auth0UserIdentity } from './auth0-user-general-dtos.schema';

export const auth0ActionEventUserDtoSchema = S.object()
  .additionalProperties(false)
  .prop('user_id', S.string())
  .prop('email', S.string())
  .prop('email_verified', S.boolean())
  .prop('username', S.string())
  .prop('phone_number', S.string())
  .prop('phone_verified', S.boolean())
  .prop('identities', S.array().items(auth0UserIdentity))
  .prop('app_metadata', S.object())
  .prop('user_metadata', S.object())
  .prop('picture', S.string())
  .prop('name', S.string())
  .prop('nickname', S.string())
  .prop('multifactor', S.array().items(S.string()))
  .prop('last_ip', S.string())
  .prop('last_login', S.string())
  .prop('logins_count', S.integer())
  .prop('blocked', S.boolean())
  .prop('given_name', S.string())
  .prop('family_name', S.string());
