export class SessionExpiredError extends Error {
  constructor(message?: string) {
    super(message || `Your session is expired. Please try to login again`);
  }
}
