export class NotAuthenticatedRequestError extends Error {
  constructor(message?: string) {
    super(message || `You are not authenticated`);
  }
}
