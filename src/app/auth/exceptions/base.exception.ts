import { BaseException } from '@lib/exceptions/base.exception';

export abstract class AuthenticationBaseException extends BaseException {
  public type = 'authentication_error';
}
