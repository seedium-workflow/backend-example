import { AuthenticationBaseException } from './base.exception';

export class NotAuthenticatedRequestException extends AuthenticationBaseException {
  code = 'not_authenticated_request';
  status = 401;
  constructor(message?: string) {
    super(message || `You are not authenticated`);
  }
}
