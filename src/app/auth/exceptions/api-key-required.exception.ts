import { AuthenticationBaseException } from './base.exception';

export class ApiKeyRequiredException extends AuthenticationBaseException {
  code = 'api_key_required';
  status = 401;
  constructor(message?: string) {
    super(message ?? `API key is required`);
  }
}
