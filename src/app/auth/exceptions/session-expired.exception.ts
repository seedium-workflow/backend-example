import { AuthenticationBaseException } from './base.exception';

export class SessionExpiredException extends AuthenticationBaseException {
  code = 'session_expired';
  status = 401;
  constructor(message?: string) {
    super(message || `Your session is expired. Please try to login again`);
  }
}
