import { isString } from '@utils';
import { AuthenticationBaseException } from './base.exception';

export class ForbiddenException extends AuthenticationBaseException {
  code = 'forbidden';
  status = 403;
  constructor(messageOrRequiredScopes: string | string[] = 'Forbidden action') {
    super(
      isString(messageOrRequiredScopes)
        ? messageOrRequiredScopes
        : `You are not authorized to do this action. You need the next permissions to make this action: ${messageOrRequiredScopes.join(
            ' ',
          )}`,
    );
  }
}
