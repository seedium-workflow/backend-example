import { AuthenticationBaseException } from './base.exception';

export class InvalidUserFromAuth0EventException extends AuthenticationBaseException {
  code = 'validation_failed';
  status = 400;
  constructor(message?: string) {
    super(
      message ||
        `Some of provided values are invalid. Please fix them and try again`,
    );
  }
}
