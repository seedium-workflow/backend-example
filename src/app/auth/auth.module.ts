import { forwardRef, Module } from '@nestjs/common';
import { UserModule } from '@app/user/user.module';
import { Auth0Controller } from './controllers';
import {
  JwtService,
  AuthService,
  Auth0Service,
  AuthenticationService,
} from './services';

@Module({
  imports: [forwardRef(() => UserModule)],
  controllers: [Auth0Controller],
  providers: [JwtService, AuthService, Auth0Service, AuthenticationService],
  exports: [JwtService, AuthService, Auth0Service, AuthenticationService],
})
export class AuthModule {}
