import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  OnModuleInit,
} from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { featuresConfig, FeaturesConfig } from '@config/features.config';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import { UserRecord } from '@app/user/interfaces';
import { AuthStrategy } from '../interfaces';
import { AuthDisabledStrategy, AuthEnabledStrategy } from './strategies';

@Injectable()
export class AuthenticationGuard implements CanActivate, OnModuleInit {
  private strategy: AuthStrategy | null = null;

  constructor(
    @Inject(featuresConfig.KEY)
    private readonly fc: FeaturesConfig,
    private readonly moduleRef: ModuleRef,
  ) {}

  public async onModuleInit(): Promise<void> {
    if (this.fc.auth) {
      this.strategy = await this.moduleRef.create(AuthEnabledStrategy);
    } else {
      this.strategy = await this.moduleRef.create(AuthDisabledStrategy);
    }
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const result = await (this.strategy as AuthStrategy).execute(context);
    const req = context
      .switchToHttp()
      .getRequest<ApplicationFastifyRequest<UserRecord>>();

    if (req.user) {
      req.metadata.setIdUser(req.user.id);
      req.sentryScope.setUser(req.user);
    }
    return result;
  }
}
