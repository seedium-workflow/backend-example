import { Injectable, Logger } from '@nestjs/common';
import { MetadataContainer } from '@modules/core/metadata';
import { RequiredBy } from '@lib/interfaces';
import { UserService } from '@app/user/services';
import {
  User,
  CompletedUser,
  NotCompletedUser,
  UserSignUpPayload,
} from '@app/user/interfaces';
import {
  Auth0EventRequestDto,
  Auth0EventUserPreSignupDto,
  Auth0EventUserPostSignupDto,
} from '../interfaces';
import { InvalidUserFromAuth0EventError } from '../errors';
import { Auth0Service } from './auth0.service';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);

  constructor(
    private readonly auth0Service: Auth0Service,
    private readonly userService: UserService,
  ) {}
  public async databasePreSignUp(
    authEventRequest: Auth0EventRequestDto,
    auth0EventUserDto: Auth0EventUserPreSignupDto,
    metadata: MetadataContainer,
  ): Promise<NotCompletedUser | Error> {
    if (!this.isValidAuthEventUser(authEventRequest, auth0EventUserDto)) {
      return new InvalidUserFromAuth0EventError('pre_signup');
    }
    return this.preSignUp(
      {
        email: auth0EventUserDto.email,
        first_name: auth0EventUserDto.given_name,
        last_name: auth0EventUserDto.family_name,
        avatar_link: auth0EventUserDto.picture,
        timezone: authEventRequest.geoip.timeZone,
        city: authEventRequest.geoip.cityName,
        country: authEventRequest.geoip.countryName,
      },
      metadata,
    );
  }
  public async databasePostSignUp(
    authEventRequest: Auth0EventRequestDto,
    auth0EventUserDto: Auth0EventUserPostSignupDto,
    metadata: MetadataContainer,
  ): Promise<User | Error | null> {
    if (!this.isValidAuthEventUser(authEventRequest, auth0EventUserDto)) {
      return new InvalidUserFromAuth0EventError('post_signup');
    }
    return this.postSignUp(
      auth0EventUserDto.user_id,
      {
        email: auth0EventUserDto.email,
        first_name: auth0EventUserDto.given_name,
        last_name: auth0EventUserDto.family_name,
        avatar_link: auth0EventUserDto.picture,
        timezone: authEventRequest.geoip.timeZone,
        city: authEventRequest.geoip.cityName,
        country: authEventRequest.geoip.countryName,
      },
      metadata,
    );
  }
  private isValidAuthEventUser(
    authEventRequest: Auth0EventRequestDto,
    auth0EventUserDto: Auth0EventUserPreSignupDto,
  ): auth0EventUserDto is RequiredBy<
    Auth0EventUserPreSignupDto,
    'email' | 'given_name' | 'family_name'
  > {
    if (!auth0EventUserDto.email) {
      this.logger.error(
        {
          auth0Request: authEventRequest,
        },
        'Wrong auth0 request',
      );
      this.logger.error(
        {
          auth0User: auth0EventUserDto,
        },
        'Wrong auth0 user',
      );
      return false;
    }
    return true;
  }

  private async preSignUp(
    userSignUpPayload: UserSignUpPayload,
    metadata: MetadataContainer,
  ): Promise<NotCompletedUser | Error> {
    return this.userService.signUp(userSignUpPayload, metadata);
  }

  private async postSignUp(
    id: string,
    userSignUpPayload: UserSignUpPayload,
    metadata: MetadataContainer,
  ): Promise<CompletedUser | Error | null> {
    const completedUser = await this.userService.finishSignUp(
      {
        email: userSignUpPayload.email,
        first_name: userSignUpPayload.first_name,
        last_name: userSignUpPayload.last_name,
        avatar_link: userSignUpPayload.avatar_link,
        timezone: userSignUpPayload.timezone,
        city: userSignUpPayload.city,
        country: userSignUpPayload.country,
        sub: id,
      },
      metadata,
    );
    if (!completedUser) {
      return null;
    }
    return completedUser;
  }
}
