import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ID } from 'nestjs-postgres';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import { AccountUserService } from '@app/relations/account-user/services';
import { BadRequestException } from '@lib/exceptions/api';
import { ForbiddenException } from '@app/auth/exceptions';
import { httpAssert } from '@utils';

@Injectable()
export class AccountGuard implements CanActivate {
  constructor(private readonly accountUserService: AccountUserService) {}

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<ApplicationFastifyRequest>();
    const idUser = req.user?.id;
    httpAssert(
      idUser,
      new Error(
        'ID user not found. Seems authentication guard was not executed before',
      ),
    );
    const idAccount = this.extractActiveAccountFromRequest(req);
    httpAssert(!!idAccount, new BadRequestException('ID account not found'));
    req.idAccount = idAccount;
    const accountUser = await this.accountUserService.retrieveByAccountAndUser(
      idAccount,
      idUser,
    );
    httpAssert(
      accountUser,
      new ForbiddenException('You are trying to access forbidden account'),
    );
    return true;
  }

  private extractActiveAccountFromRequest(
    req: ApplicationFastifyRequest,
  ): ID | null {
    return req.headers['example-account'] ?? null;
  }
}
