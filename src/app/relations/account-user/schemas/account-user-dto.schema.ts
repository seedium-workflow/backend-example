import S from 'fluent-json-schema';
import { idSchema, resourceObjectSchema } from '@lib/schemas';

export const accountUserDtoSchema = S.object()
  .additionalProperties(false)
  .prop('tenant', idSchema)
  .prop('user', idSchema)
  .extend(resourceObjectSchema);
