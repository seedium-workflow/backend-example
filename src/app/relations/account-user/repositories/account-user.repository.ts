import {
  ID,
  SoftAccountRepository,
  EntityAccountRepository,
  RepositoryDatabaseOptions,
  IList,
  RepositoryListOptionsCreated,
} from 'nestjs-postgres';
import { AccountRecord } from '@app/account/interfaces';
import { User } from '@app/user/interfaces';
import { AccountUser, AccountUserRecord } from '../interfaces';

@EntityAccountRepository({
  name: 'account_users',
})
export class AccountUserRepository extends SoftAccountRepository<
  AccountUserRecord,
  AccountUser
> {
  public async listAccountsByUserId(
    idUser: ID,
    options?: RepositoryListOptionsCreated,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<IList<AccountRecord>> {
    const accountAlias = 'accounts';
    const accountUserAlias = 'account_users';
    return this._repository.hooks.list(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select(`${accountAlias}.*`)
        .where({ user: idUser })
        .join(
          accountAlias,
          `${accountAlias}.id`,
          `${accountUserAlias}.account`,
        ),
      options,
      databaseOptions,
    );
  }
  public async retrieveByAccountAndUser(
    idAccount: ID,
    idUser: ID,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<AccountUser | null> {
    return this._repository.hooks.retrieve(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select('*')
        .where('account', idAccount)
        .where('user', idUser)
        .whereNull('deleted_at')
        .first(),
    );
  }
  public async retrieveByEmailInAccount(
    idAccount: ID,
    email: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<User | null> {
    const accountUserAlias = 'account_users';
    const userAlias = 'users';
    return this._repository.hooks.retrieve(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select(`${userAlias}.*`)
        .join(userAlias, `${userAlias}.id`, `${accountUserAlias}.user`)
        .where(`${userAlias}.email`, email)
        .whereNull(`${accountUserAlias}.deleted_at`)
        .whereNull(`${userAlias}.deleted_at`)
        .first(),
    );
  }
}
