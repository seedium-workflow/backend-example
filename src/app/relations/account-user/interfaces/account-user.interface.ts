import {
  ID,
  ForeignRef,
  IAccountResourceModel,
  IAccountRecordModel,
} from 'nestjs-postgres';
import { User } from '@app/user/interfaces';

export type AccountUserRole = 'owner';

export interface AccountUser extends IAccountResourceModel {
  user: ForeignRef<User>;
  role: AccountUserRole;
}

export interface AccountUserRecord extends IAccountRecordModel {
  user: ID;
  role: AccountUserRole;
}
