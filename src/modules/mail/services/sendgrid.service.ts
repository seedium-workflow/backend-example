import { Inject, Injectable } from '@nestjs/common';
import { MailService, MailDataRequired } from '@sendgrid/mail';
import { featuresConfig, FeaturesConfig } from '@config/features.config';
import { mailConfig, MailConfig } from '@config/mail.config';
import { SendgridSendOptions } from '../interfaces';

@Injectable()
export class SendgridService {
  private readonly mailService: MailService;
  constructor(
    @Inject(featuresConfig.KEY)
    private readonly fc: FeaturesConfig,
    @Inject(mailConfig.KEY)
    private readonly mc: MailConfig,
  ) {
    this.mailService = new MailService();
    if (this.fc.mail && this.mc.sendgrid.apiKey) {
      this.mailService.setApiKey(this.mc.sendgrid.apiKey);
    }
  }
  public async send(
    to: string | string[],
    options: SendgridSendOptions,
  ): Promise<void> {
    const mailData = this.retrievePayload(to, options);
    await this.mailService.send(mailData, Array.isArray(to));
  }
  private retrievePayload(
    to: string | string[],
    options: SendgridSendOptions,
  ): MailDataRequired | MailDataRequired[] {
    if (Array.isArray(to)) {
      return to.map((email) => ({
        ...options,
        to: email,
        from: this.mc.fromAddress,
      }));
    }
    return {
      ...options,
      to,
      from: this.mc.fromAddress,
    };
  }
}
