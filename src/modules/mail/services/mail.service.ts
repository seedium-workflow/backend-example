import { Injectable } from '@nestjs/common';
import { SendOptions } from '../interfaces';
import { SendgridService } from './sendgrid.service';

@Injectable()
export class MailService {
  constructor(private readonly sendGridService: SendgridService) {}
  public async send(
    to: string | string[],
    options: SendOptions,
  ): Promise<void> {
    return this.sendGridService.send(to, options);
  }
}
