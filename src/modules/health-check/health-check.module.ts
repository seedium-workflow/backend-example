import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { KnexModule } from '@modules/knex';
import { HealthCheckController } from './controllers';

@Module({
  imports: [TerminusModule, KnexModule.forRoot()],
  controllers: [HealthCheckController],
})
export class HealthCheckModule {}
