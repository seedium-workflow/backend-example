import { Knex } from 'knex';
import { EventBusTransaction } from '@modules/cqrs';

export interface TransactionOptions {
  knexTrx?: Knex.Transaction;
  eventBusTrx?: EventBusTransaction;
}
