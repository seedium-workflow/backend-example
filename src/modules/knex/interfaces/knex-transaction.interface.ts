import { Knex } from 'knex';

export interface KnexTransactionConfig extends Knex.TransactionConfig {
  deferred?: string[];
}
