import { Inject } from '@nestjs/common';
import { KNEX_MANAGER } from '../knex.constants';

export const InjectKnexManager = (): ((
  target: object,
  key: string | symbol,
  index?: number,
) => void) => Inject(KNEX_MANAGER);
