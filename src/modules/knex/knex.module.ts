import {
  DynamicModule,
  FactoryProvider,
  OnApplicationShutdown,
  Logger,
} from '@nestjs/common';
import knex, { Knex } from 'knex';
import { defer } from 'rxjs';
import { ConfigType } from '@nestjs/config';
import { RepositoryInjector } from '@lib/db/injectors';
import { dbConfig } from '@config/db.config';
import { handleRetryConnection } from '@utils';
import { InjectKnexManager } from '@modules/knex/decorators';
import { DatabaseHealthIndicator } from './indicators';
import { KNEX_MANAGER } from './knex.constants';
import { KnexTransactionService } from './services';

export class KnexModule implements OnApplicationShutdown {
  static forRoot(): DynamicModule {
    const logger = new Logger(KnexModule.name);
    const knexProvider: FactoryProvider = {
      provide: KNEX_MANAGER,
      inject: [dbConfig.KEY],
      useFactory: async (config: ConfigType<typeof dbConfig>) =>
        await defer(async () => {
          if (config.connection.connectionString) {
            logger.log(
              `Connecting to database ${KnexModule.hideStringCredentials(
                config.connection.connectionString,
              )}...`,
            );
          } else {
            logger.log(
              `Connecting to database ${config.connection.host}:${config.connection.port}...`,
            );
          }

          const knexInstance = knex({
            client: 'pg',
            connection: config.connection,
            pool: config.pool,
          });
          await knexInstance.raw(`select 1+1 as result`);
          return knexInstance;
        })
          .pipe(handleRetryConnection('postgres', logger))
          .toPromise(),
    };
    return {
      module: KnexModule,
      providers: [
        knexProvider,
        DatabaseHealthIndicator,
        RepositoryInjector,
        KnexTransactionService,
      ],
      exports: [
        knexProvider,
        DatabaseHealthIndicator,
        RepositoryInjector,
        KnexTransactionService,
      ],
    };
  }

  static hideStringCredentials(connectionString: string): string {
    return connectionString.replace(/\/\/.*@/, '//*****:*****@');
  }
  private readonly _logger = new Logger(KnexModule.name);
  constructor(@InjectKnexManager() private readonly _knex: Knex) {}

  async onApplicationShutdown(): Promise<void> {
    await this._knex.destroy();
    this._logger.log(`Connection to database is destroyed`);
  }
}
