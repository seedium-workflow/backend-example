import { HttpAdapterHost } from '@nestjs/core';
import { Injectable, Logger } from '@nestjs/common';
import { FastifyAdapter } from '@nestjs/platform-fastify';
import { ApplicationFastifyRequest } from '../interfaces';

@Injectable()
export class RequestLogger {
  private readonly logger = new Logger(RequestLogger.name);

  constructor(private readonly httpAdapter: HttpAdapterHost<FastifyAdapter>) {
    if (!this.httpAdapter.httpAdapter) {
      return;
    }
    const fastify = this.httpAdapter.httpAdapter.getInstance();

    fastify.addHook('onRequest', (req, _, done) => {
      this.logger.log(
        {
          reqId: req.id,
          traceId: (req as ApplicationFastifyRequest).metadata.traceId,
          req: {
            method: req.method,
            url: req.url,
            hostname: req.hostname,
            ip: req.ip,
            headers: req.headers,
            query: req.query,
            params: req.params,
          },
        },
        'incoming request',
      );
      done();
    });

    fastify.addHook('onResponse', (req, res, done) => {
      this.logger.log(
        {
          reqId: req.id,
          traceId: (req as ApplicationFastifyRequest).metadata.traceId,
          res: { statusCode: res.statusCode },
          responseTime: res.getResponseTime(),
        },
        'request completed',
      );
      done();
    });
  }
}
