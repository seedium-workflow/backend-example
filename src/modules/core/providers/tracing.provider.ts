import * as Sentry from '@sentry/node';
import { Injectable } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { FastifyRequest } from 'fastify';
import { FastifyAdapter } from '@nestjs/platform-fastify';
import { TRACEPARENT_REGEXP } from '@sentry/tracing';
import { MetadataContainer } from '../metadata';
import { ApplicationFastifyRequest } from '../interfaces';

@Injectable()
export class TracingProvider {
  constructor(private readonly httpAdapter: HttpAdapterHost<FastifyAdapter>) {
    if (!this.httpAdapter.httpAdapter) {
      return;
    }
    const fastify = this.httpAdapter.httpAdapter.getInstance();

    fastify.addHook('onRequest', (req, _, done) => {
      const parentTrace = this.extractParentTrace(
        req as unknown as FastifyRequest,
      );
      const transactionName = this.extractTransactionName(
        req as unknown as FastifyRequest,
      );
      const transaction = Sentry.startTransaction(
        {
          op: 'http.server',
          name: transactionName,
          ...parentTrace,
        },
        { request: this.extractRequestData(req as unknown as FastifyRequest) },
      );
      const sentryScope = new Sentry.Scope();
      sentryScope.setSpan(transaction);
      (req as unknown as ApplicationFastifyRequest).sentryScope = sentryScope;
      (req as unknown as ApplicationFastifyRequest).metadata =
        new MetadataContainer(transaction.traceId);

      done();
    });
    fastify.addHook('onResponse', (req, res, done) => {
      const scope = (req as unknown as ApplicationFastifyRequest).sentryScope;
      const transaction = scope.getTransaction();
      if (transaction) {
        transaction.setHttpStatus(res.statusCode);
        transaction.finish();
      }
      done();
    });
  }
  private extractParentTrace(req: FastifyRequest): Record<string, unknown> {
    const sentryTrace = req.headers['sentry-trace'];
    if (!sentryTrace) {
      return {};
    }
    if (typeof sentryTrace !== 'string') {
      return {};
    }
    const matches = sentryTrace.match(TRACEPARENT_REGEXP);
    if (!matches) {
      return {};
    }
    let parentSampled: boolean | undefined;
    if (matches[3] === '1') {
      parentSampled = true;
    } else if (matches[3] === '0') {
      parentSampled = false;
    }
    return {
      traceId: matches[1],
      parentSampled,
      parentSpanId: matches[2],
    };
  }
  private extractTransactionName(req: FastifyRequest): string {
    return `${req.routerMethod ?? req.method} ${req.routerPath ?? req.url}`;
  }
  private extractRequestData(req: FastifyRequest): Record<string, unknown> {
    return {
      headers: req.headers,
      query: req.query,
      params: req.params,
      method: req.method,
      url: req.url,
      body: req.body,
    };
  }
}
