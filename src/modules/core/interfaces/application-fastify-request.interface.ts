import { Scope } from '@sentry/types';
import { FastifyRequest } from 'fastify';
import { UserRecord } from '@app/user/interfaces';
import { MetadataContainer } from '../metadata';

export interface ApplicationFastifyRequest<User = UserRecord>
  extends FastifyRequest {
  headers: Record<string, string>;
  user?: User;
  permissions?: string[];
  idAccount?: string;
  metadata: MetadataContainer;
  sentryScope: Scope;
}
