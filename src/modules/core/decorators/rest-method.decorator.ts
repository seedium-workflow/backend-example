import { applyDecorators, HttpCode } from '@nestjs/common';
import { JSONSchema } from 'fluent-json-schema';
import { ApiBody } from '@nestjs/swagger';
import {
  ApiResponseSchema,
  HeadersSchema,
  ParamsSchema,
  QuerySchema,
  ResponseSchema,
} from '@lib/decorators/swagger';
import { Scopes } from './scopes.decorator';

type RestMethodOptions = {
  scopes?: string[];
  responses?: Record<number, ApiResponseSchema>;
  headers?: JSONSchema;
  query?: JSONSchema;
  params?: JSONSchema;
  body?: JSONSchema;
  statusCode?: number;
};
type RestMethodHandler = <TFunction extends Function, Y>(
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void;

export function RestMethod(
  options: RestMethodOptions,
  ...userDecorators: Array<MethodDecorator | ClassDecorator>
): RestMethodHandler {
  const {
    responses = {},
    scopes = [],
    headers,
    query,
    params,
    body,
    statusCode,
  } = options;
  const decorators: Array<MethodDecorator | ClassDecorator> = [];

  decorators.push(ResponseSchema(responses));
  decorators.push(Scopes(scopes));

  if (headers) {
    decorators.push(HeadersSchema(headers));
  }
  if (query) {
    decorators.push(QuerySchema(query));
  }
  if (params) {
    decorators.push(ParamsSchema(params));
  }
  if (body) {
    decorators.push(
      ApiBody({
        schema: body.valueOf(),
      }),
    );
  }
  if (statusCode) {
    decorators.push(HttpCode(statusCode));
  }

  return applyDecorators(...decorators, ...userDecorators);
}
