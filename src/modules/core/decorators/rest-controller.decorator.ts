import { applyDecorators, Controller, ControllerOptions } from '@nestjs/common';

type RestControllerOptions = string | ControllerOptions;
type RestControllerHandler = <TFunction extends Function, Y>(
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void;

export function RestController(
  options: RestControllerOptions,
  ...customDecorators: Array<MethodDecorator | ClassDecorator>
): RestControllerHandler {
  const decorators: Array<MethodDecorator | ClassDecorator> = [];
  decorators.push(
    Controller(typeof options === 'string' ? { path: options } : options),
  );
  return applyDecorators(...decorators, ...customDecorators);
}
