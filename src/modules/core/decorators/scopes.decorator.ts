import { SetMetadata, applyDecorators, ScopeOptions } from '@nestjs/common';

export const EXPECTED_SCOPES_TOKEN = Symbol.for('expected_scopes');
export const EXPECTED_SCOPES_OPTIONS_TOKEN = Symbol.for('scope_options');

export const Scopes = (
  expectedScopes: string[],
  options: ScopeOptions = {},
): MethodDecorator & ClassDecorator => {
  return applyDecorators(
    SetMetadata(EXPECTED_SCOPES_TOKEN, expectedScopes),
    SetMetadata(EXPECTED_SCOPES_OPTIONS_TOKEN, options),
  );
};
