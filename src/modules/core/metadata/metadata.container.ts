import { ID } from '@lib/db';

export class MetadataContainer {
  static cloneFrom(
    metadata: MetadataContainer,
    parentTraceId?: string,
  ): MetadataContainer {
    return new MetadataContainer(metadata.traceId, parentTraceId).setIdUser(
      metadata.idUser(),
    );
  }

  private _idUser: ID | null = null;

  get traceId(): string {
    return this._traceId;
  }

  get parentTraceId(): string | undefined {
    return this._parentTraceId;
  }

  constructor(
    private readonly _traceId: string,
    private readonly _parentTraceId?: string,
  ) {}

  public idUser(): ID | null;
  public idUser(strict: true): ID;
  public idUser(strict = false): string | null {
    if (strict && !this._idUser) {
      throw new Error('No id user found in metadata');
    }
    return this._idUser;
  }

  public setIdUser(idUser?: ID | null): this {
    this._idUser = idUser ?? null;
    return this;
  }
}
