import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ApplicationFastifyRequest } from '../interfaces';

export const Metadata = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<ApplicationFastifyRequest>();
    return req.metadata;
  },
);
