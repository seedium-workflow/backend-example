import { Catch, ExceptionFilter, Type, ArgumentsHost } from '@nestjs/common';
import { FastifyRequest, FastifyReply } from 'fastify';
import { BaseException } from '@lib/exceptions/base.exception';

@Catch(BaseException as unknown as Type)
export class HttpExceptionFilter implements ExceptionFilter<BaseException> {
  catch(exception: BaseException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<FastifyReply>();
    const req = ctx.getRequest<FastifyRequest>();

    res.status(exception.status).send({
      ...exception,
      requestId: req.id,
      message: exception.message,
    });
  }
}
