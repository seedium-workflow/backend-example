import {
  Catch,
  ExceptionFilter,
  ArgumentsHost,
  NotFoundException as NestNotFoundException,
} from '@nestjs/common';
import { FastifyRequest, FastifyReply } from 'fastify';
import { NotFoundException } from '@lib/exceptions/api';

@Catch(NestNotFoundException)
export class NotFoundExceptionFilter
  implements ExceptionFilter<NestNotFoundException>
{
  catch(_: NestNotFoundException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<FastifyReply>();
    const req = ctx.getRequest<FastifyRequest>();
    const urlInvalidException = new NotFoundException();

    res.status(urlInvalidException.status).send({
      ...urlInvalidException,
      requestId: req.id,
      message: urlInvalidException.message,
    });
  }
}
