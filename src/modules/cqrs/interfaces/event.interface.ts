import { IEvent as INestDefaultEvent } from '@nestjs/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { ID } from '@lib/db';
import { IJsonSerializable } from '@modules/cqrs';

export interface IEvent extends IJsonSerializable, INestDefaultEvent {
  id: string;
  version: string;
  traceId: string;
  metadata: MetadataContainer;
  parentMetadata: MetadataContainer;
}

export interface EventData<T> {
  object: T;
  previous_attributes?: Partial<T>;
}

export interface EventPayload<T> {
  id: string;
  version: string;
  type: string;
  created_at: number;
  request: EventRequest;
  data: EventData<T>;
}

export interface EventRequest {
  traceId: string;
  parentTraceId?: string;
  user: ID | null;
}
