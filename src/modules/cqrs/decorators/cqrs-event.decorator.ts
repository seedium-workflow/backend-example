import { applyDecorators, SetMetadata } from '@nestjs/common';
import { CQRS_EVENT_OPTIONS } from '../cqrs.constants';

export const CqrsEvent = (): (<TFunction extends Function, Y>(
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void) => applyDecorators(SetMetadata(CQRS_EVENT_OPTIONS, {}));
