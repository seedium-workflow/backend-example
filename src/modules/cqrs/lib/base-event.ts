import { randomUUID } from 'crypto';
import { now, reflect } from '@utils';
import { MetadataContainer } from '@modules/core/metadata';
import { EventRequest, EventPayload, IEvent } from '../interfaces';
import { EVENT_VERSION_METADATA } from '../cqrs.constants';

export class BaseEvent implements IEvent {
  static parseEventMessage<Payload>(message: string): EventPayload<Payload> {
    return JSON.parse(message);
  }

  static eventRequestToMetadata(eventRequest: EventRequest): MetadataContainer {
    return new MetadataContainer(
      eventRequest.traceId,
      eventRequest.parentTraceId,
    ).setIdUser(eventRequest.user);
  }

  static metadataToEventRequest(metadata: MetadataContainer): EventRequest {
    return {
      traceId: metadata.traceId,
      parentTraceId: metadata.parentTraceId,
      user: metadata.idUser(),
    };
  }

  public readonly traceId: string;
  public readonly version: string;
  public readonly type: string;
  public metadata: MetadataContainer;

  constructor(
    public readonly parentMetadata: MetadataContainer,
    public readonly id: string = randomUUID(),
  ) {
    const eventVersion =
      reflect<string>(this.constructor, EVENT_VERSION_METADATA) ?? '1';
    this.version = eventVersion;
    this.traceId = parentMetadata.traceId;
    this.metadata = parentMetadata;
  }

  public toJson(payload: unknown, previousAttributes?: unknown): string {
    let data: Record<string, unknown> = {
      object: payload,
    };
    if (previousAttributes) {
      data = {
        ...data,
        previous_attributes: previousAttributes,
      };
    }
    return JSON.stringify({
      id: this.id,
      version: this.version,
      type: this.type,
      request: BaseEvent.metadataToEventRequest(this.metadata),
      created_at: now(),
      data,
    });
  }
}
