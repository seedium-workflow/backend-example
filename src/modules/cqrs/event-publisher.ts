import { Subject } from 'rxjs';
import { IEventPublisher } from '@nestjs/cqrs';
import { IEvent, IStaticJsonSerializable, PublishOptions } from './interfaces';

export class EventPublisher<T extends IEvent = IEvent>
  implements IEventPublisher<T>
{
  constructor(private subject$: Subject<T>) {}

  public async publish(event: T, options?: PublishOptions): Promise<void> {
    const copyEvent = (
      event.constructor as IStaticJsonSerializable<new () => T, T>
    ).fromJson(event.toJson());
    if (options?.transaction) {
      await options.transaction.send(() => this.subject$.next(copyEvent));
    } else {
      this.subject$.next(copyEvent);
    }
  }
}
