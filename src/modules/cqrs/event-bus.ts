import { EventBus as NestEventBus } from '@nestjs/cqrs';
import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { CommandBus } from './command-bus';
import { EventPublisher } from './event-publisher';
import { IEvent, PublishOptions } from './interfaces';

@Injectable()
export class EventBus<
  EventBase extends IEvent = IEvent,
> extends NestEventBus<EventBase> {
  private readonly _eventPublisher: EventPublisher<EventBase>;

  constructor(commandBus: CommandBus, moduleRef: ModuleRef) {
    /* TODO temporary workaround for fix types error. Need to find more elegant solution to fix it */
    // @ts-expect-error constructor of `EventBus` accepts only exactly one `CommandBus` but not the contractor
    super(commandBus, moduleRef);
    this._eventPublisher = new EventPublisher<EventBase>(this.subject$);
  }

  public async publish<T extends EventBase>(
    event: T,
    options?: PublishOptions,
  ): Promise<void> {
    await this._eventPublisher.publish(event, options);
  }
}
