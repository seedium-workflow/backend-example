import { Module } from '@nestjs/common';
import { LoggerModule as NestjsPinoLoggerModule } from 'nestjs-pino';
import { pino } from 'pino';
import { appConfig, AppConfig } from '@config/app.config';

@Module({
  imports: [
    NestjsPinoLoggerModule.forRootAsync({
      useFactory: (config: AppConfig) => {
        const { extremeMode, ...loggerOptions } = config.logger;
        const dest = pino.destination({
          sync: !extremeMode.enabled,
          minLength: extremeMode.enabled ? extremeMode.minLength : undefined,
        });
        const logger = pino(loggerOptions, dest);

        return {
          pinoHttp: {
            logger,
            autoLogging: false,
          },
          ...loggerOptions,
        };
      },
      inject: [appConfig.KEY],
    }),
  ],
})
export class LoggerModule {}
