import { registerAs, ConfigType } from '@nestjs/config';
import { Knex } from 'knex';
import { readPackageJson } from '@utils';

export const dbConfig = registerAs('db', () => {
  const packageJson = readPackageJson();

  const connectionString = process.env.DATABASE_URL ?? '';
  const host = process.env.DATABASE_HOST ?? 'localhost';
  const port = +(process.env.DATABASE_PORT ?? 5432);
  const user = process.env.DATABASE_USERNAME ?? 'postgres';
  const password = process.env.DATABASE_PASSWORD ?? 'example';
  const database = process.env.DATABASE_NAME ?? 'main';

  let connectionOptions: Knex.PgConnectionConfig = {
    keepAlive: true,
    application_name: `${packageJson.name}@${packageJson.version}`,
  };
  if (connectionString) {
    connectionOptions = { ...connectionOptions, connectionString };
  } else {
    connectionOptions = {
      ...connectionOptions,
      host,
      port,
      user,
      password,
      database,
    };
  }

  return {
    connection: connectionOptions,
    pool: {
      min: 2,
      max: 10,
    },
    repository: {
      limit: 20,
      maxLimit: 100,
    },
  };
});

export type DBConfig = ConfigType<typeof dbConfig>;
