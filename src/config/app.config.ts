import { registerAs, ConfigType } from '@nestjs/config';
import {
  readPackageJson,
  parseRuntimeBuildInfo,
  coerceStringToBoolean,
} from '@utils';

export const appConfig = registerAs('app', () => {
  const packageJson = readPackageJson();
  const runtimeBuildInfo = parseRuntimeBuildInfo();

  return {
    name: 'example',
    environment: process.env.APP_ENVIRONMENT ?? 'local',
    port: +(process.env.PORT ?? 3000),
    address: process.env.ADDRESS ?? 'localhost',
    origin: process.env.APP_ORIGIN ?? 'http://localhost:3000',
    version: packageJson.version as string,
    commitShortSha: runtimeBuildInfo.commitShortSha,
    logger: {
      enabled: true,
      redact: ['req.headers.authorization'],
      mixin: () => ({ version: packageJson.version }),
      extremeMode: {
        minLength: +(process.env.APP_LOGGER_EXTREME_MODE_MIN_LENGTH ?? 4096),
        enabled:
          coerceStringToBoolean(process.env.LOGGER_EXTREME_MODE) ?? false,
      },
    },
  };
});

export type AppConfig = ConfigType<typeof appConfig>;
