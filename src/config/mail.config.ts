import { registerAs, ConfigType } from '@nestjs/config';

export const mailConfig = registerAs('mail', () => ({
  fromAddress: process.env.MAIL_FROM_ADDRESS ?? 'no-reply@dev.example.com',
  sendgrid: {
    apiKey: process.env.MAIL_SENDGRID_API_KEY,
  },
}));

export type MailConfig = ConfigType<typeof mailConfig>;
