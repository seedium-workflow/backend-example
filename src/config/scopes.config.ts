import { ConfigType, registerAs } from '@nestjs/config';

export const scopes = {
  auth: {},
  profile: {
    retrieve: 'profile:retrieve',
    update: 'profile:update',
  },
  accounts: {
    list: 'accounts:list',
    create: 'accounts:create',
    retrieve: 'accounts:retrieve',
    update: 'accounts:update',
    delete: 'accounts:delete',
  },
  invitations: {
    list: 'invitations:list',
    create: 'invitations:create',
    retrieve: 'invitations:retrieve',
    update: 'invitations:update',
    delete: 'invitations:delete',
  },
};

export const scopesConfig = registerAs<{
  default: string[];
  roles: {
    owner: string[];
  };
}>('scopes', () => ({
  default: [
    scopes.profile.retrieve,
    scopes.profile.update,
    scopes.accounts.list,
    scopes.accounts.create,
    scopes.accounts.retrieve,
    scopes.accounts.update,
    scopes.accounts.delete,
  ],
  roles: {
    owner: [
      scopes.invitations.list,
      scopes.invitations.create,
      scopes.invitations.retrieve,
      scopes.invitations.update,
      scopes.invitations.delete,
    ],
  },
}));

export type ScopesConfig = ConfigType<typeof scopesConfig>;
