export const isError = <T extends Error>(
  maybeError: unknown,
  errorInstance?: unknown,
): maybeError is T =>
  errorInstance
    ? maybeError instanceof (errorInstance as Function)
    : maybeError instanceof Error;
