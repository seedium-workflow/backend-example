import { OperatorFunction, pipe } from 'rxjs';
import { debounceTime, groupBy, map, mergeMap } from 'rxjs/operators';

export const mapDebounceByGroup = <T, R>(
  time: number,
  groupSelector: (event: T) => unknown,
  mapSelector: (event: T) => R,
): OperatorFunction<T, R> => {
  return pipe(
    groupBy(groupSelector),
    mergeMap(($groups) => $groups.pipe(debounceTime(time), map(mapSelector))),
  );
};
