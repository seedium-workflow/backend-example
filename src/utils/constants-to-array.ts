export const constantsToArray = (constants: Record<string, string>): string[] =>
  Object.values(constants);
