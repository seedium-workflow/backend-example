import S from 'fluent-json-schema';

export const errorResponseSchema = S.object()
  .additionalProperties(false)
  .prop('request_id', S.string())
  .prop('message', S.string())
  .prop('type', S.string())
  .prop('code', S.string());

export const validationErrorSchema = S.object()
  .additionalProperties(false)
  .prop('name', S.string())
  .prop('keyword', S.string())
  .prop('message', S.string());

export const validationErrorResponseSchema = S.object()
  .additionalProperties(false)
  .prop('errors', S.array().items(validationErrorSchema))
  .extend(errorResponseSchema);
