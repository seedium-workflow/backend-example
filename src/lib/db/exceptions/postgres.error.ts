import { DatabaseError } from 'pg';
import { isObjectLike } from '@utils';

export class PostgresError extends Error {
  static isPostgresDatabaseError(err: unknown): err is DatabaseError {
    return isObjectLike(err) && 'length' in err;
  }
  public readonly code: string | undefined;
  public readonly detail: string | undefined;
  public readonly constraint: string | undefined;
  public readonly schema: string | undefined;
  public readonly column: string | undefined;
  public readonly dataType: string | undefined;
  public readonly file: string | undefined;
  public readonly hint: string | undefined;
  public readonly internalPosition: string | undefined;
  public readonly internalQuery: string | undefined;
  public readonly line: string | undefined;
  public readonly position: string | undefined;
  public readonly routine: string | undefined;
  public readonly severity: string | undefined;
  public readonly table: string | undefined;
  public readonly where: string | undefined;

  constructor(databaseError: DatabaseError) {
    super(databaseError.message);
    this.code = databaseError.code;
    this.detail = databaseError.detail;
    this.constraint = databaseError.constraint;
    this.schema = databaseError.schema;
    this.column = databaseError.column;
    this.dataType = databaseError.dataType;
    this.file = databaseError.file;
    this.hint = databaseError.hint;
    this.internalPosition = databaseError.internalPosition;
    this.internalQuery = databaseError.internalQuery;
    this.line = databaseError.line;
    this.routine = databaseError.routine;
    this.severity = databaseError.severity;
    this.table = databaseError.table;
    this.where = databaseError.where;
  }
}
