import { Optional } from '@nestjs/common';
import { Knex } from 'knex';
import {
  IRepository,
  RepositoryDatabaseOptions,
  IPaginationBuilder,
  RepositoryListOptionsCreated,
  SelectRepositoryDatabaseOptions,
  RepositoryDataOptions,
} from '@lib/db';
import { RepositoryInjector } from '@lib/db/injectors/repository-injector';
import { InjectKnexManager } from '@modules/knex';
import {
  ID,
  IList,
  IResourceObject,
  OmitDefaultResourceFields,
} from '../interfaces';
import { SoftRepositoryInterceptor } from '../interceptors';
import { PostgresRepository } from '../postgres-repository';

export class SoftRepository<
  TRecord extends IResourceObject,
  TResult extends IResourceObject,
> implements IRepository<TRecord, TResult>
{
  protected readonly _repository: PostgresRepository<TRecord, TResult>;
  protected readonly _softInterceptorRepository: SoftRepositoryInterceptor<
    TRecord,
    TResult
  >;
  get repository(): PostgresRepository<TRecord, TResult> {
    return this._repository;
  }
  constructor(
    @InjectKnexManager() manager: Knex,
    repositoryInjector: RepositoryInjector,
    @Optional() pagination?: IPaginationBuilder<TRecord, TResult>,
  ) {
    this._repository = new PostgresRepository(
      this,
      repositoryInjector,
      manager,
      pagination,
    );
    this._softInterceptorRepository = new SoftRepositoryInterceptor(
      this._repository,
    );
  }
  public onModuleInit(): Promise<void> {
    return this._repository.onModuleInit();
  }
  public async list<T extends TResult>(
    options?: RepositoryListOptionsCreated,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Promise<IList<T>> {
    return this._repository.hooks.list(
      this._softInterceptorRepository.list(options, databaseOptions),
      options,
      databaseOptions,
    );
  }
  public async create(
    entity: OmitDefaultResourceFields<TRecord>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord> {
    return this._repository.hooks.create(
      this._softInterceptorRepository.create(entity, databaseOptions),
    );
  }
  public async retrieve<T extends TResult>(
    id: ID,
    options?: RepositoryDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Promise<T | null> {
    return this._repository.hooks.retrieve(
      this._softInterceptorRepository.retrieve(id, options, databaseOptions),
      options,
      databaseOptions,
    );
  }
  public async update(
    id: ID,
    entity: Partial<OmitDefaultResourceFields<TRecord>>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord | null> {
    return this._repository.hooks.update(
      this._softInterceptorRepository.update(id, entity, databaseOptions),
    );
  }
  public async delete(
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord | null> {
    return this._repository.hooks.delete(
      this._softInterceptorRepository.delete(id, databaseOptions),
    );
  }
  public async restore(
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord | null> {
    const [record] = await this._softInterceptorRepository.restore(
      id,
      databaseOptions,
    );
    if (!record) {
      return null;
    }
    return record;
  }
}
