import { Optional } from '@nestjs/common';
import { Knex } from 'knex';
import { IPaginationBuilder, IRawRepository } from '@lib/db';
import { RepositoryInjector } from '@lib/db/injectors/repository-injector';
import { InjectKnexManager } from '@modules/knex';
import { PostgresRepository } from '../postgres-repository';

export class RawRepository<TRecord, TResult>
  implements IRawRepository<TRecord, TResult>
{
  protected readonly _repository: PostgresRepository<TRecord, TResult>;
  get repository(): PostgresRepository<TRecord, TResult> {
    return this._repository;
  }
  constructor(
    @InjectKnexManager() manager: Knex,
    repositoryInjector: RepositoryInjector,
    @Optional() pagination?: IPaginationBuilder<TRecord, TResult>,
  ) {
    this._repository = new PostgresRepository(
      this,
      repositoryInjector,
      manager,
      pagination,
    );
  }
  public onModuleInit(): Promise<void> {
    return this._repository.onModuleInit();
  }
}
