import { Optional } from '@nestjs/common';
import { Knex } from 'knex';
import {
  AliasableRepositoryDatabaseOptions,
  IPaginationBuilder,
  IRepository,
  RepositoryDatabaseOptions,
  RepositoryDataOptions,
  RepositoryListOptionsCreated,
  SelectRepositoryDatabaseOptions,
} from '@lib/db';
import {
  AccountRepositoryInterceptor,
  SoftRepositoryInterceptor,
} from '@lib/db/interceptors';
import { RepositoryInjector } from '@lib/db/injectors/repository-injector';
import { PostgresRepository } from '@lib/db/postgres-repository';
import { addPrefixColumn } from '@lib/db/utils';
import { InjectKnexManager } from '@modules/knex';
import {
  ID,
  IList,
  IAccountRecordModel,
  IAccountResourceModel,
  OmitDefaultAccountResourceFields,
} from '../interfaces';

export class SoftAccountRepository<
  TRecord extends IAccountRecordModel,
  TResult extends IAccountResourceModel,
> implements IRepository<TRecord, TResult>
{
  protected readonly _repository: PostgresRepository<TRecord, TResult>;
  protected readonly _accountInterceptorRepository: AccountRepositoryInterceptor<
    TRecord,
    TResult
  >;
  protected readonly _softInterceptorRepository: SoftRepositoryInterceptor<
    TRecord,
    TResult
  >;
  get repository(): PostgresRepository<TRecord, TResult> {
    return this._repository;
  }
  constructor(
    @InjectKnexManager() manager: Knex,
    repositoryInjector: RepositoryInjector,
    @Optional() pagination?: IPaginationBuilder<TRecord, TResult>,
  ) {
    this._repository = new PostgresRepository(
      this,
      repositoryInjector,
      manager,
      pagination,
    );
    this._softInterceptorRepository = new SoftRepositoryInterceptor(
      this._repository,
    );
    this._accountInterceptorRepository = new AccountRepositoryInterceptor(
      this._softInterceptorRepository,
    );
  }
  public onModuleInit(): Promise<void> {
    return this._repository.onModuleInit();
  }
  public async list<T extends TResult>(
    idAccount: ID,
    options?: RepositoryListOptionsCreated,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Promise<IList<T>> {
    return this._repository.hooks.list(
      this._accountInterceptorRepository.list(
        idAccount,
        options,
        databaseOptions,
      ),
      options,
      databaseOptions,
    );
  }
  public async create(
    idAccount: ID,
    entity: OmitDefaultAccountResourceFields<TRecord>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord> {
    return this._repository.hooks.create(
      this._accountInterceptorRepository.create(
        idAccount,
        entity,
        databaseOptions,
      ),
    );
  }
  public async retrieve<T extends TResult>(
    idAccount: ID,
    id: ID,
    options?: RepositoryDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Promise<T | null> {
    return this._repository.hooks.retrieve(
      this._accountInterceptorRepository.retrieve(
        idAccount,
        id,
        options,
        databaseOptions,
      ),
      options,
      databaseOptions,
    );
  }
  public async update(
    idAccount: ID,
    id: ID,
    entity: Partial<OmitDefaultAccountResourceFields<TRecord>>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord | null> {
    return this._repository.hooks.update(
      this._accountInterceptorRepository.update(
        idAccount,
        id,
        entity,
        databaseOptions,
      ),
    );
  }
  public async delete(
    idAccount: ID,
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord | null> {
    return (await this._repository.hooks.delete(
      this._accountInterceptorRepository.delete(idAccount, id, databaseOptions),
    )) as unknown as TRecord;
  }
  public async restore(
    idAccount: ID,
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Promise<TRecord | null> {
    const [record] = await this._softInterceptorRepository
      .restore(id, databaseOptions)
      .where(addPrefixColumn('account', databaseOptions?.alias), idAccount);
    if (!record) {
      return null;
    }
    return record;
  }
}
