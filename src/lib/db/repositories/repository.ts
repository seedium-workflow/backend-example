import { Optional } from '@nestjs/common';
import { Knex } from 'knex';
import {
  IRepository,
  RepositoryDatabaseOptions,
  IPaginationBuilder,
  RepositoryListOptionsCreated,
  SelectRepositoryDatabaseOptions,
  RepositoryDataOptions,
} from '@lib/db';
import { RepositoryInjector } from '@lib/db/injectors/repository-injector';
import { InjectKnexManager } from '@modules/knex';
import { PostgresRepository } from '../postgres-repository';
import {
  ID,
  IList,
  HardResourceObject,
  OmitDefaultHardResourceFields,
} from '../interfaces';

export class Repository<
  TRecord extends HardResourceObject,
  TResult extends HardResourceObject,
> implements IRepository<TRecord, TResult>
{
  protected readonly _repository: PostgresRepository<TRecord, TResult>;
  get repository(): PostgresRepository<TRecord, TResult> {
    return this._repository;
  }
  constructor(
    @InjectKnexManager() manager: Knex,
    repositoryInjector: RepositoryInjector,
    @Optional() pagination?: IPaginationBuilder<TRecord, TResult>,
  ) {
    this._repository = new PostgresRepository(
      this,
      repositoryInjector,
      manager,
      pagination,
    );
  }
  public onModuleInit(): Promise<void> {
    return this._repository.onModuleInit();
  }
  public async list<T extends TResult>(
    options?: RepositoryListOptionsCreated,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Promise<IList<T>> {
    return this._repository.hooks.list(
      this._repository.list(options, databaseOptions),
      options,
      databaseOptions,
    );
  }
  public async create(
    entity: OmitDefaultHardResourceFields<TRecord>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord> {
    return this._repository.hooks.create(
      this._repository.create(entity, databaseOptions),
    );
  }
  public async retrieve<T extends TResult>(
    id: ID,
    options?: RepositoryDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Promise<T | null> {
    return this._repository.hooks.retrieve(
      this._repository.retrieve(id, options, databaseOptions),
      options,
      databaseOptions,
    );
  }
  public async update(
    id: ID,
    entity: Partial<OmitDefaultHardResourceFields<TRecord>>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<TRecord | null> {
    return this._repository.hooks.update(
      this._repository.update(id, entity, databaseOptions),
    );
  }
  public async delete(
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<void> {
    await this._repository.hooks.delete(
      this._repository.delete(id, databaseOptions),
    );
  }
}
