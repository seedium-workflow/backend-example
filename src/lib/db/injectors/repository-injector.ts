import { ModuleRef } from '@nestjs/core';
import { Injectable } from '@nestjs/common';
import { TypeRepository, IRepository } from '@lib/db';

@Injectable()
export class RepositoryInjector {
  constructor(private readonly _moduleRef: ModuleRef) {}
  public injectRepository(token: TypeRepository): IRepository {
    return this._moduleRef.get(token, { strict: false });
  }
}
