import { Knex } from 'knex';
import {
  AliasableRepositoryDatabaseOptions,
  IAbstractRepository,
  RepositoryDatabaseOptions,
  RepositoryDataOptions,
  RepositoryListOptionsCreated,
  SelectRepositoryDatabaseOptions,
} from '@lib/db';
import { now } from '@utils';
import { addPrefixColumn } from '@lib/db/utils';
import { ID, IResourceObject, OmitDefaultResourceFields } from '../interfaces';

export class SoftRepositoryInterceptor<
  TRecord extends IResourceObject,
  TResult extends IResourceObject,
> implements IAbstractRepository<TRecord, TResult>
{
  constructor(
    private readonly _repository: IAbstractRepository<TRecord, TResult>,
  ) {}
  public list(
    listOptions?: RepositoryListOptionsCreated,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository
      .list(listOptions, databaseOptions)
      .whereNull(addPrefixColumn('deleted_at', databaseOptions?.alias));
  }
  public create(
    entity: OmitDefaultResourceFields<TRecord>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository.create(entity, databaseOptions);
  }
  public retrieve(
    id: ID,
    options?: RepositoryDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository
      .retrieve(id, options, databaseOptions)
      .whereNull(addPrefixColumn('deleted_at', databaseOptions?.alias));
  }
  public update(
    id: ID,
    entity: Partial<OmitDefaultResourceFields<TRecord>>,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository
      .update(id, entity, databaseOptions)
      .whereNull(addPrefixColumn('deleted_at', databaseOptions?.alias));
  }
  public delete(
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    return (
      this._repository
        /* eslint-disable-next-line */
        .queryBuilder<any>(databaseOptions?.transaction)
        .where(addPrefixColumn('id', databaseOptions?.alias), id)
        .whereNull(addPrefixColumn('deleted_at', databaseOptions?.alias))
        .update({ deleted_at: now() })
        .returning('*')
    );
  }
  public restore(
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    return (
      this._repository
        /* eslint-disable-next-line */
        .queryBuilder<any>(databaseOptions?.transaction)
        .where(addPrefixColumn('id', databaseOptions?.alias), id)
        .whereNotNull(addPrefixColumn('deleted_at', databaseOptions?.alias))
        .update({ deleted_at: null })
        .returning('*')
    );
  }
  public queryBuilder<TOverrideRecord extends TRecord>(
    trx?: Knex.Transaction,
  ): Knex.QueryBuilder<TOverrideRecord, TResult> {
    return this._repository.queryBuilder(trx);
  }
}
