import { Knex } from 'knex';
import {
  AliasableRepositoryDatabaseOptions,
  IAbstractRepository,
  RepositoryDatabaseOptions,
  RepositoryDataOptions,
  RepositoryListOptionsCreated,
  SelectRepositoryDatabaseOptions,
} from '@lib/db';
import { addPrefixColumn } from '@lib/db/utils';
import {
  ID,
  OmitDefaultAccountResourceFields,
  IAccountRecordModel,
  IAccountResourceModel,
} from '../interfaces';

export class AccountRepositoryInterceptor<
  TRecord extends IAccountRecordModel,
  TResult extends IAccountResourceModel,
> implements IAbstractRepository<TRecord, TResult>
{
  constructor(
    private readonly _repository: IAbstractRepository<TRecord, TResult>,
  ) {}
  public list(
    idAccount: ID,
    listOptions?: RepositoryListOptionsCreated,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository
      .list(listOptions, databaseOptions)
      .where(addPrefixColumn('account', databaseOptions?.alias), idAccount);
  }
  public create(
    idAccount: ID,
    entity: OmitDefaultAccountResourceFields<TRecord>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository.create(
      {
        ...entity,
        account: idAccount,
      },
      databaseOptions,
    );
  }
  public retrieve(
    idAccount: ID,
    id: ID,
    options?: RepositoryDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository
      .retrieve(id, options, databaseOptions)
      .where(addPrefixColumn('account', databaseOptions?.alias), idAccount);
  }
  public update(
    idAccount: ID,
    id: ID,
    entity: Partial<OmitDefaultAccountResourceFields<TRecord>>,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    return this._repository
      .update(id, entity, databaseOptions)
      .where(addPrefixColumn('account', databaseOptions?.alias), idAccount);
  }
  public delete(
    idAccount: ID,
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord, TResult> {
    return this._repository
      .delete(id, databaseOptions)
      .where(addPrefixColumn('account', databaseOptions?.alias), idAccount);
  }
  public queryBuilder<TOverrideRecord extends TRecord>(
    trx?: Knex.Transaction,
  ): Knex.QueryBuilder<TOverrideRecord, TResult> {
    return this._repository.queryBuilder(trx);
  }
}
