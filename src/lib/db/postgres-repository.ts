import { OnModuleInit } from '@nestjs/common';
import { Knex } from 'knex';
import { now } from '@utils';
import { RepositoryReflector } from '@lib/db/repository-reflector';
import { CrudRepositoryHook } from '@lib/db/crud-repository-hook';
import type { QueryResult } from 'pg';
import {
  ID,
  OmitDefaultHardResourceFields,
  IRepository,
  IPaginationBuilder,
  RepositoryDatabaseOptions,
  IAbstractRepository,
  RepositoryListOptionsCreated,
  RepositoryDataOptions,
  SelectRepositoryDatabaseOptions,
  IRawRepository,
  IUpdateTimestamp,
  ICreateTimestamp,
  AliasableRepositoryDatabaseOptions,
} from './interfaces';
import { ExpandInjector, RepositoryInjector } from './injectors';
import { ExpandHook, IncludeHook } from './hooks';
import { PagePagination } from './paginations';
import { RepositoryBuilder } from './repository-builder';
import { addPrefixColumn, IdFactory } from './utils';

export class PostgresRepository<TRecord, TResult>
  implements IAbstractRepository<TRecord, TResult>, OnModuleInit
{
  public readonly hooks: CrudRepositoryHook<TRecord, TResult>;
  public readonly builder: RepositoryBuilder<TRecord, TResult>;
  public readonly idFactory: IdFactory | undefined;

  protected readonly _reflector: RepositoryReflector<TRecord, TResult>;

  protected readonly _expandInjector: ExpandInjector;

  protected readonly _includeHook: IncludeHook<TRecord, TResult>;
  protected readonly _expandHook: ExpandHook;

  constructor(
    repository: IRepository | IRawRepository<TRecord, TResult>,
    repositoryInjector: RepositoryInjector,
    protected readonly _manager: Knex,
    protected readonly _pagination: IPaginationBuilder<
      TRecord,
      TResult
    > = new PagePagination(),
  ) {
    this._reflector = new RepositoryReflector(repository.constructor);
    this.builder = new RepositoryBuilder(this._pagination);
    this._expandInjector = new ExpandInjector(
      repositoryInjector,
      this._reflector.expandable,
    );
    this._includeHook = new IncludeHook();
    this._expandHook = new ExpandHook(this._expandInjector);
    this.hooks = new CrudRepositoryHook(
      this,
      this._reflector,
      this._includeHook,
      this._expandHook,
      this._pagination,
    );
    if (this._reflector.prefix) {
      this.verifyPrefixLength(this._reflector.prefix);
      this.idFactory = new IdFactory(this._reflector.prefix);
    }
  }
  public async onModuleInit(): Promise<void> {
    this._expandInjector.onModuleInit();
  }
  public list(
    {
      limit,
      page,
      sort = [],
      created,
      ...filterOptions
    }: RepositoryListOptionsCreated = {},
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Knex.QueryBuilder<TRecord> {
    const queryBuilder = this.queryBuilder(databaseOptions?.transaction);
    queryBuilder.select(
      addPrefixColumn('*', databaseOptions?.alias),
      ...this._includeHook.runInline(
        {
          factoryQueryBuilder: () =>
            this.queryBuilder(databaseOptions?.transaction),
          factoryRawBuilder: () =>
            this.rawBuilder(databaseOptions?.transaction),
        },
        this._includeHook.filterCanIncludeInline(databaseOptions?.include),
      ),
    );
    this.builder.buildFilterQuery(
      queryBuilder,
      () => this.rawBuilder(databaseOptions?.transaction),
      filterOptions,
      {
        strategies: databaseOptions?.filter,
        alias: databaseOptions?.alias,
      },
    );
    this.builder.buildPaginationQuery(queryBuilder, limit, page);
    this.builder.buildSortQuery(queryBuilder, sort, {
      alias: databaseOptions?.alias,
    });
    this.builder.buildDateFilterQuery(queryBuilder, created, {
      alias: databaseOptions?.alias,
    });
    return queryBuilder;
  }
  public create(
    entity: OmitDefaultHardResourceFields<TRecord>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    /* eslint-disable-next-line */
    return this.queryBuilder<any>(databaseOptions?.transaction)
      .insert({
        id: this.idFactory?.generate(),
        ...entity,
        ...this.getCreateTimestamps(),
      })
      .returning('*');
  }
  public retrieve(
    id: ID,
    options?: RepositoryDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<TRecord, TResult>,
  ): Knex.QueryBuilder<TRecord> {
    const queryBuilder = this.queryBuilder(databaseOptions?.transaction);
    queryBuilder
      .select(
        addPrefixColumn('*', databaseOptions?.alias),
        ...this._includeHook.runInline(
          {
            factoryQueryBuilder: () =>
              this.queryBuilder(databaseOptions?.transaction),
            factoryRawBuilder: () =>
              this.rawBuilder(databaseOptions?.transaction),
          },
          this._includeHook.filterCanIncludeInline(databaseOptions?.include),
        ),
      )
      .where(addPrefixColumn('id', databaseOptions?.alias), id)
      .first();
    return queryBuilder;
  }
  public update(
    id: ID,
    entity: Partial<OmitDefaultHardResourceFields<TRecord>>,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord> {
    /* eslint-disable-next-line */
    return this.queryBuilder<any>(databaseOptions?.transaction)
      .where(addPrefixColumn('id', databaseOptions?.alias), id)
      .update({
        ...entity,
        ...this.getUpdateTimestamps(),
      })
      .returning('*');
  }
  public delete(
    id: ID,
    databaseOptions?: RepositoryDatabaseOptions &
      AliasableRepositoryDatabaseOptions,
  ): Knex.QueryBuilder<TRecord, TResult> {
    return this.queryBuilder(databaseOptions?.transaction)
      .where(addPrefixColumn('id', databaseOptions?.alias), id)
      .del();
  }
  public queryBuilder<TOverrideRecord extends TRecord>(
    trx?: Knex.Transaction,
  ): Knex.QueryBuilder<TOverrideRecord, TResult> {
    const queryBuilder = this._manager<TOverrideRecord, TResult>(
      this._reflector.table,
    );
    if (trx) {
      queryBuilder.transacting(trx);
    }
    return queryBuilder;
  }

  public rawBuilder(
    trx?: Knex.Transaction,
  ): Knex.RawBuilder<TRecord, QueryResult>;
  public rawBuilder(trx?: Knex.Transaction): (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    valueOrSql: any,
    ...bindings: readonly Knex.RawBinding[]
  ) => Knex.Raw<QueryResult> {
    return (
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      valueOrSql: any,
      ...bindings: readonly Knex.RawBinding[]
    ): Knex.Raw<QueryResult> => {
      const rawBuilder = this._manager.raw(valueOrSql, ...bindings);
      if (trx) {
        rawBuilder.transacting(trx);
      }
      return rawBuilder;
    };
  }

  public getCreateTimestamps(): ICreateTimestamp {
    return {
      created_at: now(),
      updated_at: now(),
    };
  }
  public getUpdateTimestamps(): IUpdateTimestamp {
    return {
      updated_at: now(),
    };
  }
  private verifyPrefixLength(prefix: string): void {
    if (prefix.length > 30) {
      throw new Error(
        `The prefix "${prefix}" has exceeded maximum limit of column id. Maximum length should 30 chars or less`,
      );
    }
  }
}
