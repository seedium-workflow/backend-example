import {
  EntityRepositoryOptions,
  ExpandableOptions,
  ListExpandableOptions,
  accountRepositoryExpandListStrategy,
  accountRepositoryExpandRetrieveStrategy,
} from '@lib/db';
import { EntityRepository } from './entity-repository.decorator';

const isFieldExpandableOptions = (
  maybeFieldExpandableOptions: unknown | undefined,
): maybeFieldExpandableOptions is ListExpandableOptions => {
  if (!maybeFieldExpandableOptions) {
    return false;
  }
  return !!(maybeFieldExpandableOptions as ListExpandableOptions).repository;
};

const transformExpandableOptions = (
  expandable?: ExpandableOptions,
): ExpandableOptions | undefined => {
  if (!expandable) {
    return undefined;
  }
  return Object.fromEntries(
    Object.entries(expandable).map(
      ([property, expandableOptionsOrTypeRepository]) => {
        if (isFieldExpandableOptions(expandableOptionsOrTypeRepository)) {
          return [
            property,
            {
              listStrategy: accountRepositoryExpandListStrategy,
              retrieveStrategy: accountRepositoryExpandRetrieveStrategy,
              ...expandableOptionsOrTypeRepository,
            },
          ];
        }
        return [
          property,
          {
            listStrategy: accountRepositoryExpandListStrategy,
            retrieveStrategy: accountRepositoryExpandRetrieveStrategy,
            repository: expandableOptionsOrTypeRepository,
          },
        ];
      },
    ),
  );
};

const transformOptions = (
  options: EntityRepositoryOptions,
): EntityRepositoryOptions => ({
  ...options,
  expandable: transformExpandableOptions(options.expandable),
});

export const EntityAccountRepository = (
  options: EntityRepositoryOptions,
): ClassDecorator => {
  return (target) => EntityRepository(transformOptions(options))(target);
};
