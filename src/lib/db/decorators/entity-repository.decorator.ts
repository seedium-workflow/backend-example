import { EntityRepositoryOptions, ENTITY_REPOSITORY_OPTIONS } from '@lib/db';

export const EntityRepository = (
  options: EntityRepositoryOptions,
): ClassDecorator => {
  return (target) => {
    Reflect.defineMetadata(ENTITY_REPOSITORY_OPTIONS, options, target);
  };
};
