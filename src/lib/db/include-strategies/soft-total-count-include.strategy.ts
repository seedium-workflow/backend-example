import { omit } from '@utils';
import {
  IListOptionsCreated,
  HardResourceObject,
  CanIncludeList,
  QueryBuilderMap,
} from '../interfaces';
import { Include } from '../decorators';
import { sanitizeFilterOptions } from '../utils';

@Include('total_count')
export class SoftTotalCountIncludeStrategy<
  TRecord extends HardResourceObject,
  TResult extends HardResourceObject,
> implements CanIncludeList<TRecord, TResult, number>
{
  constructor(private readonly _options?: IListOptionsCreated) {}
  public async list({
    queryBuilder,
  }: QueryBuilderMap<TRecord, TResult>): Promise<number> {
    const filterOptions = this.extractFilterOptions(this._options);
    const sanitizedFilter = sanitizeFilterOptions(filterOptions);
    const result = await queryBuilder
      .where(sanitizedFilter)
      .whereNull('deleted_at')
      .count('*')
      .first();
    return parseInt(result.count);
  }
  protected extractFilterOptions(
    options: IListOptionsCreated = {},
  ): Record<string, unknown> {
    return omit(
      options,
      'page',
      'limit',
      'include',
      'expand',
      'sort',
      'created',
    );
  }
}
