export interface IList<T> {
  /**
   * An array containing the actual response elements, paginated by any request parameters.
   */
  data: T[];

  /**
   * The total number of items available. This value is not included by default,
   * but you can request it by specifying ?include[]=total_count
   */
  total_count?: number;
}

export interface IComplexDateFilter {
  /**
   * Return values where the created field is after this timestamp.
   */
  gt?: number;

  /**
   * Return values where the created field is after or equal to this timestamp.
   */
  gte?: number;

  /**
   * Return values where the created field is before this timestamp.
   */
  lt?: number;

  /**
   * Return values where the created field is before or equal to this timestamp.
   */
  lte?: number;
}

/**
 * A filter on the list based on this object field. The value can
 * be a string with an integer Unix timestamp, or it can be a
 * dictionary with the following options:
 */
export type IDateFilter = number | IComplexDateFilter;

export interface IDataOptions {
  /*
   * Expand field with full resource object
   * */
  expand?: string[];

  /*
   * Include additional values into the response
   * */
  include?: string[];
}

export interface IListOptions extends IDataOptions {
  /*
   * A page number of objects to be returned
   * */
  page?: number;

  /*
   * Pass array of fields on which should sort
   * */
  sort?: string[];

  /*
   * A limit on the number of objects to be returned. Limit can range between 1 and 100 items.
   */
  limit?: number;

  /*
   * A column name with value that should be filtered
   * */
  [filterColumn: string]: string | number | unknown;
}

export interface IListOptionsCreated extends IListOptions {
  created?: IDateFilter;
}

export type DefaultListInclude = 'total_count';

/*export interface ICursorList<T> extends IList<T> {
  /!*
   * Whether or not there are more elements available after this set. If false, this set comprises the end of the list.
   * *!/
  has_more: boolean;
}

export interface ListCursorOptionsCreated extends IListOptionsCreated {
  /!*
   * A cursor for use in pagination. starting_after is an object ID that defines your place in the list. For instance, if you make a list request and receive 100 objects, ending with obj_foo, your subsequent call can include starting_after=obj_foo in order to fetch the next page of the list.
   * *!/
  starting_after?: string;

  /!*
   * A cursor for use in pagination. ending_before is an object ID that defines your place in the list. For instance, if you make a list request and receive 100 objects, starting with obj_bar, your subsequent call can include ending_before=obj_bar in order to fetch the previous page of the list.
   * *!/
  ending_before?: string;
}*/
