import { ForeignRef, ID, IResourceObject } from '.';

export interface IAccountResourceModel<
  T extends IResourceObject = IResourceObject,
> extends IResourceObject {
  account: ForeignRef<T>;
}

export interface IAccountRecordModel extends IResourceObject {
  account: ID;
}

export type DeletedHardResourceObject = Omit<IResourceObject, 'deleted_at'>;
