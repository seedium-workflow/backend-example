import { Knex } from 'knex';
import { OnModuleInit } from '@nestjs/common';
import { PostgresRepository } from '../postgres-repository';
import { IList } from './list.interface';
import { HardResourceObject } from './resource.interface';

export interface IAbstractRepository<TRecord, TResult> {
  list(...args: unknown[]): Knex.QueryBuilder<TRecord>;
  create(...args: unknown[]): Knex.QueryBuilder<TRecord>;
  retrieve(...args: unknown[]): Knex.QueryBuilder<TRecord>;
  update(...args: unknown[]): Knex.QueryBuilder<TRecord>;
  delete(...args: unknown[]): Knex.QueryBuilder<TRecord>;
  queryBuilder<TOverrideRecord extends TRecord>(
    trx?: Knex.Transaction,
  ): Knex.QueryBuilder<TOverrideRecord, TResult>;
}

export interface IRepository<
  TRecord extends HardResourceObject = HardResourceObject,
  TResult extends HardResourceObject = HardResourceObject,
> extends OnModuleInit {
  repository: PostgresRepository<TRecord, TResult>;
  list(...args: unknown[]): Promise<IList<TResult>>;
  create(...args: unknown[]): Promise<TRecord>;
  retrieve(...args: unknown[]): Promise<TResult | null>;
  update(...args: unknown[]): Promise<TRecord | null>;
  delete(...args: unknown[]): Promise<TRecord | null | void>;
}

export interface IRawRepository<TRecord, TResult> extends OnModuleInit {
  repository: PostgresRepository<TRecord, TResult>;
}
