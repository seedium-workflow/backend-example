import { Knex } from 'knex';
import { IPaginationBuilder, IList, HardResourceObject } from '../interfaces';

export class UnlimitedPagination<
  TRecord extends HardResourceObject,
  TResult extends HardResourceObject,
> implements IPaginationBuilder<TRecord, TResult>
{
  public buildQuery(
    queryBuilder: Knex.QueryBuilder<TRecord, TResult>,
    limit: number,
    page: number,
  ): void {
    if (limit || page) {
      limit = limit ?? 20;
      page = page ?? 0;
      const offset = page * limit;
      queryBuilder.offset(offset).limit(limit);
    }
  }
  public prepareListResponse<TResult>(data: TResult[]): IList<TResult> {
    return {
      data,
    };
  }
}
