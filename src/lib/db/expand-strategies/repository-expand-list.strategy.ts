import { ExpandStrategy } from '@lib/db';

export const repositoryExpandListStrategy: ExpandStrategy = (
  repository,
  record,
  { foreignField, expand, databaseOptions },
) => repository.list({ [foreignField]: record.id, expand }, databaseOptions);
