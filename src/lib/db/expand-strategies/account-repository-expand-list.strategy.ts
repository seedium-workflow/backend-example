import { ExpandStrategy } from '@lib/db';

export const accountRepositoryExpandListStrategy: ExpandStrategy = (
  repository,
  record,
  { foreignField, expand, databaseOptions },
) =>
  repository.list(
    record.account,
    { [foreignField]: record.id, expand },
    databaseOptions,
  );
