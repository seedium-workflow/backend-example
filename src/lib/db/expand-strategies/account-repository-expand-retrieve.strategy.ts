import { ExpandStrategy } from '@lib/db';

export const accountRepositoryExpandRetrieveStrategy: ExpandStrategy = (
  repository,
  record,
  { localField, expand, databaseOptions },
) =>
  repository.retrieve(
    record.account,
    record[localField],
    { expand },
    databaseOptions,
  );
