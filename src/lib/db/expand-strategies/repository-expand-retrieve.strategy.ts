import { ExpandStrategy } from '@lib/db';

export const repositoryExpandRetrieveStrategy: ExpandStrategy = (
  repository,
  record,
  { localField, expand, databaseOptions },
) => repository.retrieve(record[localField], { expand }, databaseOptions);
