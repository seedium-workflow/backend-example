import { ApiBaseException } from './api-base.exception';

export class BadRequestException extends ApiBaseException {
  public code = 'bad_request';
  public status = 400;
  constructor(message?: string) {
    super(message || 'Invalid request parameters');
  }
}
