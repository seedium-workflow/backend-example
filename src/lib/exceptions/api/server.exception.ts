import { ApiBaseException } from './api-base.exception';

export class ServerException extends ApiBaseException {
  public code = 'server_exception';
  public status = 500;

  constructor(err: Error) {
    super(err.message);
  }
}
