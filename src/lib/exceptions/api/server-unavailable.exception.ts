import { ApiBaseException } from './api-base.exception';

export class ServerUnavailableException extends ApiBaseException {
  public code = 'server_unavailable_exception';
  public status = 503;
  constructor(message?: string) {
    super(message);
  }
}
