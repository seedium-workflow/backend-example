const { resolve } = require('path');
const { generateApi } = require('swagger-typescript-api');

generateApi({
  /* default 'api.ts' */
  name: 'index.ts',

  /* fix up small errors in the swagger source definition */
  patch: true,

  /* path to swagger schema */
  input: resolve(process.cwd(), process.argv[2] || 'openapi.json'),

  /* url to swagger schema */
  // url: string;

  /* swagger schema JSON */
  // spec: import("swagger-schema-official").Spec;

  /* path to folder where will been located the created api module */
  output: resolve(__dirname, '../dist'),

  /* path to folder containing templates(default : ./ scr / templates) */
  templates: resolve(__dirname, './templates'),

  /* generate all "enum" types as union types(T1 | T2 | TN)(default: false) */
  generateUnionEnums: true,

  /* generate type definitions for API routes(default: false) */
  generateRouteTypes: true,

  /* do not generate an API class */
  // generateClient?: boolean;

  /* generated http client type */
  httpClientType: 'fetch',

  /* use "default" response status code as success response too.
  some swagger schemas use "default" response status code as success response type by default. */
  defaultResponseAsSuccess: true,

  /* generate additional information about request responses
  also add typings for bad responses */
  generateResponses: true,

  /* generate js api module with declaration file(default: false) */
  toJS: true,

  /* determines which path index should be used for routes separation */
  // moduleNameIndex?: number;

  /* users operation's first tag for route separation */
  moduleNameFirstTag: true,

  /* disabled SSL check */
  // disableStrictSSL?: boolean;

  /* disabled Proxy */
  // disableProxy?: boolean;

  /* generate separated files for http client, data contracts, and routes(default: false) */
  // modular?: boolean;

  /* extract request params to data contract(Also combine path params and query params into one object) */
  // extractRequestParams: true,

  /* extract request body type to data contract */
  // extractRequestBody: true,

  /* prettier configuration */
  // prettier?: object;

  /* Output only errors to console(default: false) */
  // silent?: boolean;

  /* default type for empty response schema(default: "void") */
  // defaultResponseType?: boolean;

  /* Ability to send HttpClient instance to Api constructor */
  singleHttpClient: false,
  cleanOutput: false,
  enumNamesAsValues: false,

  // hooks?: Partial <Hooks>;

  /* extra templates */
  // extraTemplates?: { name: string; path: string }[];
})
  .catch((e) => console.error(e));
