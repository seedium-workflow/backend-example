## @example/js-client

> HTTP Rest API client

### Install

1. Add to your environment variable EXAMPLE_NPM_TOKEN. Value is your Gitlab personal token.
   For more information how to get the personal token, please follow this link https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
   Required scopes for token is only api.

1. Install package

Run script

```shell
yarn add @example/js-client
```

Install specific version

```shell
yarn add @example/js-client@x.x.x[-short_commit_sha]
```

### Usage

Base exmaple

```js
import { Api } from '@example/js-client';

const api = new Api({
    // For more configurations parameters looks to .d.ts
    baseUrl?: string,
    baseApiParams?: {
        secure?: boolean,
        path: string,
        type?: ContentType,
        query?: QueryParamsType,
        format?: ResponseFormat,
        body?: unknown,
        baseUrl?: string,
        cancelToken?: CancelToken,
    }
});

/*
Api instance includes domain-specific modules (example: accounts, contacts, messages, etc.)
*/
api.accounts
api.contacts
api.messages

/*
Methods (list, create, etc.) may vary for different domains. Look `openapi` documentations.
*/
await api.accounts.list();
await api.accounts.create();
await api.accounts.update();
await api.accounts.delete();
```

Using intefaces

```js
import { Account } from '@example/js-client';

const payload: Account.AccountCreate.RequestBody = {
  name: 'Account',
};
```
