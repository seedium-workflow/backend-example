import { ManagementClient } from 'auth0';

const managementDomain =
  process.env.AUTH0_MANAGEMENT_DOMAIN ?? 'example-local.au.auth0.com';

let managementClient: ManagementClient;

export const getManagementClient = (): ManagementClient => {
  if (managementClient) {
    return managementClient;
  }
  managementClient = new ManagementClient({
    domain: process.env.AUTH0_DOMAIN ?? 'example-local.au.auth0.com',
    clientId: process.env.AUTH0_CLIENT_ID ?? 'RgGmtHp4vnHY9dwVbHuA27a4Re3DwB78',
    clientSecret:
      process.env.AUTH0_CLIENT_SECRET ??
      'CumHCq2sL3OOzjTNCL1YVvql8E-Ie141FWF0oQK80cUrI0gbGDI2_Hw9bTo1vMvp',
    audience: `https://${managementDomain}/api/v2/`,
    scope: [
      'read:users',
      'update:users',
      'delete:users',
      'read:users_app_metadata',
      'update:users_app_metadata',
      'delete:users_app_metadata',
      'create:users_app_metadata',
      'read:roles',
      'update:roles',
      'create:user_tickets',
    ].join(' '),
  });
  return managementClient;
};
