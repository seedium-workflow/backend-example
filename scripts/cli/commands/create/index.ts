import { getCommandDirOptions } from '../../helpers';

export const command = 'create <resource>';
export const desc = 'Create resource';
export const builder = function (yargs) {
  yargs.commandDir('openapi', getCommandDirOptions());
  yargs.commandDir('auth0-user', getCommandDirOptions());
};
export const handler = function () {};
