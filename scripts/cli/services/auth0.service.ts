import { User, UserData } from 'auth0';
import { getManagementClient } from '../providers';

export const createAuth0User = async (userData: UserData): Promise<User> => {
  return await getManagementClient().createUser({
    connection: 'example',
    ...userData,
  });
};

export const getAuth0User = async (idAuth0: string): Promise<User | null> => {
  try {
    return await getManagementClient().getUser({
      id: idAuth0,
    });
  } catch (err) {
    return null;
  }
};
